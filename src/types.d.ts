/**
 * These are the BARE minimum res used by IronLibsUi
 */
interface IClientResources
{
    Yes : string,
    No : string,
    Ok : string,
    Cancel : string,
    Loading : string,
    Error : string,
    ErrorDuringWork : string,
    OperationSuccessful : string,
    AnErrorForProperty : string,
    ConfirmationRequest : string,
    NoItemToDisplay : string
    AuthenticationProblem : string
}
