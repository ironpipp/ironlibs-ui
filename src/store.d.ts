/**
 * CommonLib library dealing with local/session storage
 * (runs in browser)
 */
export declare module IronLibsStore {
    module Local {
        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        function GetPrefix(): string;
        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        function SetPrefix(Prefix: string): void;
        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        function Get(key: string): any;
        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        function Set(key: string, value: any): void;
        function Remove(key: string): any;
        function GetAllKeys(): string[];
        function Clear(): void;
        function Each(fn: (key: string, value: any) => void): void;
        function IsPresent(key: string): boolean;
    }
    module Session {
        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        function GetPrefix(): string;
        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        function SetPrefix(Prefix: string): void;
        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        function Get(key: string): any;
        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        function Set(key: string, value: any): void;
        function Remove(key: string): any;
        function GetAllKeys(): string[];
        function Clear(): void;
        function Each(fn: (key: string, value: any) => void): void;
        function IsPresent(key: string): boolean;
    }
}
