/// <reference types="jquery" />
/**
 * CommonLib library dealing with DOM and HTML
 * (runs in browser, requires JQuery library)
 */
export declare module IronLibsUi {
    /**
     * Returns a string with escaped HTML characters
     *
     * @method EscapeHtml
     * @param html {string} The plain string to escape
     * @return {string} The string with HTML ESCAPES
     */
    function EscapeHtml(html: string): string;
    /**
     * Returns a string with unescaped HTML characters
     *
     * @method UnescapeHtml
     * @param html {string} A string with HTML ESCAPES
     * @return {string} The decoded string
     */
    function UnescapeHtml(html: string): string;
    /**
     * If the tag's value is NOT EMPTY and is NOT an URL then prepends urlPrefix (defaults to "http://") to it
     *
     * @method EnsureInputValueIsUrl
     * @param $inputTag {JQuery} a JQuery wrapped INPUT TAG
     * @param [urlPrefix] {string} the string to prepend to the input value if it's not an url
     */
    function EnsureInputValueIsUrl($inputTag: JQuery, urlPrefix?: string): void;
    /**
     * Returns TRUE if the DOM document is actually exposed to the user
     * (the browser tab is active)
     *
     * @method IsDocumentVisible
     * @return boolean
     */
    function IsDocumentVisible(): boolean;
    /**
     * If the current browser is Internet Explorer, returns its version, NULL otherwise
     *
     * @method InternetExplorerVersion
     * @return number | NULL
     */
    function InternetExplorerVersion(): number;
    /**
     * Checks if the page has been restored after a browser restart WITHOUT re-executing the request
     * to the server. This is useful to be sure that old scripts are ever executed!
     * WARNING: Requires an HIDDEN INPUT tag with id="requestId" with a random value at each rendering to work
     *
     * @method IsPageFresh
     * @return {boolean} TRUE if the page is fresh or the check can't be executed, FALSE if the page is proven to be OLD.
     */
    function IsPageFresh(): boolean;
    /**
     * Returns a structure describing the browsing device.
     * The detection is performed by the CSS ENGINE, so reflects the MEDIA QUERIES rules of the different devices types
     *
     * @return {IsDesktop : boolean; IsTablet : boolean; IsPhone : boolean; IsHorizontal : boolean; IsVertical : boolean}
     * @method GetBrowsingDeviceType
     */
    function GetBrowsingDeviceType(): {
        IsDesktop: boolean;
        IsTablet: boolean;
        IsPhone: boolean;
        IsHorizontal: boolean;
        IsVertical: boolean;
    };
    /**
     * Returns the wrapped Document object of the IFRAME found under the specified selector
     *
     * @method GetIframeInnerDocument
     */
    function GetIframeInnerDocument($iframe: JQuery): JQuery;
    /**
     * Chrome browser on MOBILE devices (at the moment) when user over-scrolls up the page
     * a RELOAD PAGE button appears and activates when user releases the touch,
     * actually reloading the page.
     * This functions checks the BODY scroll situation and prevents this.
     *
     * @method PreventPullToRefresh
     */
    function PreventPullToRefresh(): void;
    function ShowOldBrowserMsg(): void;
    /**
     * Changes the TAG TYPE of the passed element keeping the attributes and data() values
     *
     * @method ChangeElementType
     * @param $el {JQuery} the elements to be changed
     * @param newType {string} the new tag name
     * @return the newly created elements
     */
    function ChangeElementType($el: JQuery, newType: string): JQuery;
    /**
     * Copy ALL the style COMPUTED properties of nodeSource to nodeDest
     *
     * @method CloneNodeStyle
     * @param nodeSource {HTMLElement} a DOM node (not jQuery object)
     * @param nodeDest  {HTMLElement} a DOM node (not jQuery object)
     */
    function CloneNodeStyle(nodeSource: HTMLElement, nodeDest: HTMLElement): void;
    /**
     * Generates pretty HTML rendering a boolean value (using Bootstrap badge)
     *
     * @method RenderBool
     * @param {boolean} boolValue
     * @return {string} the rendered HTML string
     */
    function RenderBool(boolValue: boolean): string;
    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-ONLY formatted string
     *
     * @method RenderDate
     * @return {string} the rendered HTML string
     */
    function RenderDate(date: any): string;
    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-AND-TIME formatted string
     *
     * @method RenderDateTime
     * @return {string} the rendered HTML string
     */
    function RenderDateTime(date: any): string;
    /**
     * Renders a Bootstrap toggle initialized with the passed value
     *
     * @method RenderBoolSwitcher
     * @return {string} the rendered HTML string
     */
    function RenderBoolSwitcher(boolValue: boolean, inputName?: string, inputId?: string): string;
    /**
     * Renders a spinner image tag
     *
     * @method RenderSpinner
     * @return {string} the rendered HTML string
     */
    function RenderSpinner(): string;
    /**
     * If the passed element is contained in a JqueryUI opened dialog, closes it
     *
     * @method CloseContainingPopup
     * @param $el {JQuery} the element to be checked
     */
    function CloseContainingPopup($el: JQuery | HTMLElement): void;
    /**
     * Fast helper function which returns a new SPAN element
     *
     * @method $s
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the span
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $s(content?: string | JQuery[] | JQuery, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new DIV element
     *
     * @method $d
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $d(content?: string | JQuery[] | JQuery, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new IMG element
     *
     * @method $i
     * @param src The attribute SRC of the image
     * @param alt The attribute ALT of the image
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $i(src?: string, alt?: string, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new A element
     *
     * @method $a
     * @param href The attribute HREF of the A tag
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $a(href?: string, content?: string | JQuery[] | JQuery, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new SELECT element
     *
     * @method $sel
     * @param values An array of STRING or SelectListItem
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $sel(values: string[] | {
        Text: string;
        Value: string;
        Selected?: boolean;
    }[], options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Returns an <OPTION> element
     *
     * @method $opt
     */
    function $opt(value?: string, text?: string, selected?: boolean, disabled?: boolean): JQuery;
    /**
     * Returns a list of <OPTION> elements for an ARRAY of data
     *
     * @method $opts
     */
    function $opts(items: any[], propForValue?: string | ((any: any) => string), propForText?: string | ((any: any) => string), propForSelected?: string | ((any: any) => boolean), propForDisabled?: string | ((any: any) => boolean)): JQuery;
    /**
     * Fast helper function which returns a new TD element
     *
     * @method $td
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the TD node
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $td(content: string | JQuery[] | JQuery, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new TR element
     *
     * @method $tr
     * @param [values] A simple array of object(STRING or NODE or an ARRAY of nodes) representing the content
     *                 of the respective TD subnodes to be inserted.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $tr(values: any[], options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new TABLE element
     *
     * @method $t
     * @param [content] If an array of array of object(STRING or NODE or an ARRAY of nodes) inserts all TR and TD subnodes.
     *                  If a simple array inserts only empty TR subnodes.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $t(content: any[], options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Fast helper function which returns a new STYLE element
     * Useful to COMPUTE the CSS and add/update it in the page dynamically
     *
     * @method $style
     * @param [css] The text containing the CSS rules
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $style(css: string, options1?: string | string[] | any, options2?: string | string[] | any): JQuery;
    /**
     * Forces the browser to load (and cache!) the passed image.
     * Callbacks are called upon success or error.
     *
     * @param {string} url
     * @param {(width: string, height: string, time: number, img: HTMLImageElement) => void} callback
     * @param {(time: number, img: HTMLImageElement) => void} callbackError
     * @param {boolean} disableCache
     * @method PreLoadImageWithClbk
     */
    function PreLoadImageWithClbk(url: string, callback?: (width: string, height: string, time: number, img: HTMLImageElement) => void, callbackError?: (time: number, img: HTMLImageElement) => void, disableCache?: boolean): void;
    /**
     * Executes the passed JS script
     *
     * @param script {string}
     * @param $appendScriptTagTo {JQuery}
     * @method ExecuteJsScript
     */
    function ExecuteJsScript(script: string, $appendScriptTagTo?: JQuery): void;
    /**
     * Loads from network end EXECUTES in page a SCRIPT loading the specified url
     * When loading and execution are done, calls the CALLBACK if specified
     *
     * @method LoadJsLink
     * @param src Can be an URL or a NOT already inserted HTMLScriptElement tag
     * @param async
     * @param callback
     * @param appendTagTo
     * @return {JQuery} The created and inserted tag
     */
    function LoadJsLink(src: string | HTMLScriptElement, async: boolean, callback?: (HTMLScriptElement: any) => void, appendTagTo?: JQuery): void;
    /**
     * Loads end APPLIES in page a LINK (css) tag loading the specified url
     * When loading and apply are done, calls the CALLBACK if specified
     *
     * @method LoadCssLink
     * @param src Can be an URL or a NOT already inserted HTMLLinkElement tag
     * @param callback
     * @return {JQuery} The created and inserted tag
     */
    function LoadCssLink(src: string | HTMLLinkElement, callback?: (HTMLLinkElement: any) => void): JQuery;
    /**
     * Binds a callback to be called when the CSS of the specified LINK tag(s) has been loaded
     * WARNING: linkTags MUST be already inserted in page!
     *
     * @method BindOnLoadCssLink
     * @param linkTags
     * @param callback
     */
    function BindOnLoadCssLink(linkTags: JQuery, callback: (JQuery: any) => void): void;
    /**
     * Given a TEMPLATE string and a MODEL executes the binding and returns the result string
     * Actually uses Kendo Templates syntax
     *
     * @method RenderTemplate
     * @param {JQuery | string} template The template string or a TAG holding it
     * @param model The object holding the properties to be used for the binding
     * @return {string} The result of the template execution
     */
    function RenderTemplate(template: JQuery | string, model: any): string;
    /**
     * Replace every tag content marked with "data-localize" (encode to html) or "data-localize-html" (don't encode) attributes
     * with the App.Res localized string, or with the KEY if not found.
     *
     * @param $where {JQuery} The container to limit the search for tags to be localized
     * @method ReplaceLocalizedTagsContent
     */
    function ReplaceLocalizedTagsContent($where: JQuery | string): void;
    /**
     * Notifies the user with a non modal alert message
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @param asError {boolean} If TRUE renders as a RED message
     * @method Notify
     */
    function Notify(message: string | {
        MessageText: string;
        PropertyName: string;
    }, subMessage?: string, asError?: boolean): void;
    /**
     *  Wraps Notify() with SUCCESS type
     *
     * @param message  {string | {MessageText : string, PropertyName : string}} ads
     * @param subMessage {string}
     * @method NotificateInfo
     */
    function NotificateInfo(message: string | {
        MessageText: string;
        PropertyName: string;
    }, subMessage?: string): void;
    /**
     *  Wraps Notify() with ERROR type
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @method NotificateErrors
     */
    function NotificateErrors(message: string | {
        MessageText: string;
        PropertyName: string;
    }, subMessage?: string): void;
    function CloseAllNotifications(): void;
    /**
     * When user tries to change page (hitting BACK button or a link or closing the browser)
     * it's possible to PROMPT him with a message (message is NOT displayed in Firefox ver >= 4)
     *
     * @method PreventWindowExitWithMessage
     * @param msg the textual message to show NATIVELY to user
     * @param {function} [boolFunction] If passed a function to control the prevention:
     *        if returns FALSE user can exit normally, otherwise he will be prompted!
     */
    function PreventWindowExitWithMessage(msg: string, boolFunction?: () => boolean): void;
    /**
     * Permits to exit the window without any PROMPT.
     * (Cancels registration performed with PreventWindowExitWithMessage() )
     *
     * @method AllowWindowExit
     */
    function AllowWindowExit(): void;
    /**
     * Detects the current REAL visible document size
     *
     * @return {{w : number, h : number}}
     * @method GetBrowserSize
     */
    function GetBrowserSize(): {
        w: number;
        h: number;
    };
    /**
     * IF What is a JQuery object returns the encoded HTML of the JQuery element itself (not only its content)
     * IF What is a string then HTML-encodes it
     *
     * @param what what to encode
     * @return {string} the encoded string
     * @method ToHtml
     */
    function ToHtml(what: JQuery | string): string;
    /**
     * The default Spinner image url
     *
     * @static
     * @property {string} DefaultSpinnerImageUrl
     */
    let DefaultSpinnerImageUrl: string;
    /**
     * The default Spinner SMALL image url
     *
     * @static
     * @property {string} DefaultSmallSpinnerImageUrl
     */
    let DefaultSmallSpinnerImageUrl: string;
    /**
     * The default settings for page block effect
     *
     * @static
     * @property DefaultPageBlockingStyle
     */
    let DefaultPageBlockingStyle: {
        message: string;
        css: {
            border: string;
            backgroundColor: string;
            color: string;
        };
        overlayCSS: {
            backgroundColor: string;
            opacity: number;
        };
    };
    /**
     * The default settings for element block effect
     *
     * @static
     * @property DefaultElementBlockingStyle
     */
    let DefaultElementBlockingStyle: {
        message: string;
        css: {
            border: string;
            backgroundColor: string;
            color: string;
            width: string;
        };
        overlayCSS: {
            backgroundColor: string;
            opacity: number;
        };
    };
    /**
     * Helper returning an HREF address doing nothing (useful for <a> tags)
     *
     * @method EmptyHref
     * @return {string}
     */
    function EmptyHref(): string;
    /**
     * Modally LOCKS the entire page preventing any user interaction
     * @param options Eventual object to override default options
     * @method BlockPage
     */
    function BlockPage(options?: any): void;
    /**
     * Unlocks the page (cancels the previous BlockPage() call)
     *
     * @method UnblockPage
     */
    function UnblockPage(): void;
    /**
     * The currently locked (with BlockElement()) elements
     *
     * @static
     * @property BlockedElements
     */
    let BlockedElements: HTMLElement[];
    /**
     * Modally LOCKS the passed elements preventing any user interaction
     *
     * @param $els {JQuery} The elements to lock
     * @param options Eventual object to override default options
     * @method BlockElement
     */
    function BlockElement($els: JQuery, options?: any): void;
    /**
     * Unlocks the passed elements (cancels the previous BlockElement() call)
     *
     * @param $els {JQuery} The elements to unlock
     * @method UnblockElement
     */
    function UnblockElement($els: JQuery): void;
    /**
     * Unlocks all previously locked elements (cancels the previous BlockElement() call)
     *
     * @method UnblockAllElements
     */
    function UnblockAllElements(): void;
    /**
     * Returns TRUE if the pressed key represents a NUMBER (or movement), FALSE otherwise
     *
     * @param e th Event object passed to registered callback
     * @return boolean
     * @method UserPressedANumber
     */
    function UserPressedANumber(e: JQueryKeyEventObject): boolean;
    /**
     * CommonLib library dealing with MODAL windows
     * (runs in browser, requires JQuery library)
     *
     * @class IronLibsUi.Modal
     */
    module Modal {
        /**
         * The HTML template for any modal
         *
         * @static
         * @property htmlTemplate
         */
        let htmlTemplate: string;
        interface IShowDialogOptions {
            content: string | JQuery;
            titleContent?: string | JQuery;
            showHeader?: boolean;
            showFooter?: boolean;
            footerContent?: string | JQuery;
            modalTemplate?: string;
            height?: string;
            width?: string;
            closeOnEsc?: boolean;
            closeOnOverlayClick?: boolean;
            clbkOnClose?: ($dialog: JQuery) => void;
        }
        /**
         * Opens a modal popup. Supports various parameters
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @param options {IShowDialogOptions}
         * @method ShowDialog
         */
        function ShowDialog(options: IShowDialogOptions): JQuery;
        interface IShowConfirmOptions {
            title?: string;
            message: string | JQuery;
            buttons: {
                text: string;
                closeOnClick: boolean;
                htmlAttributes: any;
                onClick?: (eventObject: JQuery.Event) => any;
            }[];
            otherDialogOptions?: any;
        }
        /**
         * Helper wrapping ShowDialog() showing 2 buttons
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @method ShowConfirm
         * @param msg {string | JQuery}
         * @param onYes {Function}
         * @param onNo {Function}
         * @param yesText {string}
         * @param noText {string}
         * @param titleText {string}
         * @param otherDialogOptions
         */
        function ShowConfirm(msg: string | JQuery, onYes?: (eventObject: JQueryEventObject) => any, onNo?: (eventObject: JQueryEventObject) => any, yesText?: string, noText?: string, titleText?: string, otherDialogOptions?: any): JQuery;
        /**
         * Helper wrapping ShowDialog() showing 2 buttons
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @method ShowConfirm
         * @param options {IShowConfirmOptions}
         */
        function ShowConfirm(options: IShowConfirmOptions): JQuery;
        /**
         *
         * In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure.
         *
         * @param msg
         * @param onOk
         * @param onCancel
         * @param yesText
         * @param cancelText
         * @param titleText
         * @param showCancel
         * @param valueRequired
         * @param valueObscured
         */
        function ShowPrompt(msg: string, onOk?: (val: string, eventObject: JQuery.Event) => any, onCancel?: (eventObject: JQuery.Event) => any, yesText?: string, cancelText?: string, titleText?: string, showCancel?: boolean, valueRequired?: boolean, valueObscured?: boolean): any;
        function ShowPrompt(options: IShowConfirmOptions): any;
        /**
         * Hides a modal shown with the "ShowXXX" method
         *
         * @method HideDialog
         * @return {boolean} TRUE if the modal has been FOUND and HID, FALSE otherwise
         */
        function HideDialog(): boolean;
        /**
         * Unbinds and destroys all dialogs, also if closed (they remain in the body)
         *
         * @method DestroyAllDialogs
         */
        function DestroyAllDialogs(): void;
    }
}
