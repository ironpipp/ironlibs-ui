//  The resources dictionary can be continuously extended adding stuff to its interface
//  Import in browser only the RES-XX.js for the language you need
var App;
(function (App) {
    App.Res = {
        Yes: "Yes",
        No: "No",
        Ok: "Ok",
        Cancel: "Cancel",
        Loading: "Loading...",
        Error: "Error",
        ErrorDuringWork: "An error has occurred during processing",
        AnErrorForProperty: "Error for the property",
        ConfirmationRequest: "Confirm",
        OperationSuccessful: "Operation completed successfully!",
        NoItemToDisplay: "No item",
        AuthenticationProblem: "Authentication problem"
    };
})(App || (App = {}));
//# sourceMappingURL=RES-en.js.map