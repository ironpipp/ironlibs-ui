//  The resources dictionary can be continuously extended adding stuff to its interface
//  Import in browser only the RES-XX.js for the language you need
var App;
(function (App) {
    App.Res = {
        Yes: "Si",
        No: "No",
        Ok: "Ok",
        Cancel: "Annulla",
        Loading: "Caricamento...",
        Error: "Errore",
        ErrorDuringWork: "E' avvenuto un errore durante l'elaborazione",
        AnErrorForProperty: "Errore per la proprietà",
        ConfirmationRequest: "Conferma",
        OperationSuccessful: "Operazione eseguita con successo!",
        NoItemToDisplay: "Nessun elemento",
        AuthenticationProblem: "Problema di autenticazione"
    };
})(App || (App = {}));
//# sourceMappingURL=RES-it.js.map