
interface JQueryStaticExtended extends JQueryStatic
{
    unblockUI : (opt? : any)=> JQueryStatic,
    blockUI : (opt? : any)=> JQueryStatic,
    browser : any,
    cookie : any,
    notify(msg : any, type : any): JQuery;
}


interface JQueryExtended extends JQuery
{
    block(options : any): JQueryStatic;
    unblock(): JQuery;
    modal(options? : any): JQuery;
    ToHtml($el : JQuery) : string;
}

declare module App
{
    export var Res : IClientResources;
}

declare function printStackTrace(opt : any) : string;
