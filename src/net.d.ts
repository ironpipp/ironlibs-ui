/// <reference types="jquery" />
/**
 * CommonLib library dealing with networking
 * (runs in browser, requires JQuery library)
 */
export declare module IronLibsNet {
    /**
     * Returns TRUE if url starts with a common networking protocol
     *
     * @method IsAbsoluteUrl
     * @param {string} url
     * @returns {boolean}
     */
    function IsAbsoluteUrl(url: string): boolean;
    /**
     * Considers the passed string a URL.
     * Returns the relative host.
     * If the url is RELATIVE returns the CURRENT host
     */
    function GetUrlDomain(url: string): string;
    /**
     * Returns the (absolute or relative) url WITHOUT DOUBLE SLASHES.
     * Doesn't check the last character (can end WITH or WITHOUT '/')
     *
     * @method SanitizeUrl
     * @param url the url to sanitize
     */
    function SanitizeUrl(url: string): string;
    /**
     * Decodes all components of a query string to a plain object.
     * Supports multiple occurrences of the same parameter (as arrays values)
     *
     * @method UrlToObject
     * @param url (not mandatory) the part of the url containing the encoded parameters (defaults to window.location.search.substring(1) )
     * @return the parameters MAP
     */
    function UrlToObject(url?: string): object;
    /**
     * Converts all object properties to an encoded query string
     * (Does the opposite of UrlToObject)
     * Supports multiple occurrences of the same parameter (as arrays values)
     *
     * @method ObjectToUrl
     */
    function ObjectToUrl(obj: object): string;
    /**
     * Checks the "Notifies" and shows messages to the user according to their type
     *
     * @method ShowNotifiesInModel
     * @param model Can be an XHR response or the model returned from an AjaxCall
     * @param $parentForPropertyErrors?
     * @param invalidateInputField?
     */
    function ShowNotifiesInModel(model: any, $parentForPropertyErrors?: JQuery, invalidateInputField?: ($input: JQuery, propertyName: string, message: string) => void): void;
    /**
     * EXTENSION of jQuery ajax settings
     */
    interface IAjaxSettings extends JQueryAjaxSettings {
        authorizationError?: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => any;
        threatInvalidDataInSuccess?: boolean;
        $parentForPropertyErrors?: JQuery;
        invalidateInputField?: ($input: JQuery, propertyName: string, message: string) => void;
    }
    let LogoffUrl: string;
    /**
     * The default Ajax options
     *
     * @static
     * @property {IAjaxSettings} AjaxDefaultOptions
     */
    let AjaxDefaultOptions: IAjaxSettings;
    /***
     * Make an AJAX call
     *
     * @method Ajax
     * @param  {IAjaxSettings} option options to configure the call
     */
    function Ajax(option: IAjaxSettings): JQueryXHR;
    /**
     * Makes a NON-AJAX call, so can reach different and UNTRUSTED domains (no CORS blocks).
     * PERMITS to be notified upon SUCCESS or ERROR, along with the (eventually JSON parsed) response.
     * LIMITATION: doesn't SEND (eventually already present) and CREATE (eventually received new) cookies for the url domain
     *
     * @param url The url (untrusted) to be called
     * @param data Eventual data to be sent
     * @param onEnd callback receiving an ERROR or the DATA upon success
     * @param parseAsJSON If TRUE the received data is parsed to JSON, otherwise it's passed to callback as-is
     */
    function CallUntrustedWithoutCookies(url: string, data?: any, onEnd?: (err: Error, data: any) => void, parseAsJSON?: boolean): void;
    /**
     * Makes a NON-AJAX call, so can reach different and UNTRUSTED domains (no CORS blocks).
     * PERMITS to be notified upon SUCCESS or ERROR, along with the (eventually JSON parsed) response.
     * PROS: Differently from "CallUntrustedWithoutCookies" the call SENDS (eventually already present) and CREATES (eventually received new) cookies for the url domain.
     * LIMITATION 1: The called resource MUST return an HTML page with a SCRIPT notifying the caller about the successful response
     *               (so the callee must be AWARE of the particular response to give)
     * LIMITATION 2: The called resource MUST accept a "requestId" parameter to be passed back to the caller
     * LIMITATION 3: The only ERROR type returned is TIMEOUT: we can't detect the real reason of the failure (StatusCode),
     *               simply the caller will never be notified about the success, so to detect the error the timeout is the only way...
     *
     * Example code of the callee to be responded:
     * if (parent != null && parent != window)
     * {
     *   parent.postMessage(JSON.stringify(
     *       {
     *           messageType: "PageLoaded",
     *           requestId: "@Html.Raw((ViewBag.RequestId as string).Replace("\"", "\\\""))",
     *           otherData : {a: 11, b: 22}
     *       }), "*");
     * }
     *
     * @param url The url (untrusted) to be called
     * @param onEnd callback receiving an ERROR or the DATA upon success
     * @param timeoutMs
     */
    function CallUntrustedWithCookies(url: string, onEnd?: (err: Error, data: any) => void, timeoutMs?: number): void;
    /**
     * To be used in response to a call performed with "CallUntrustedWithCookies"
     *
     * @param requestId The same requestId passed to the called url
     * @param data An eventual data to pass as response
     * @param messageType An eventual message type
     */
    function NotifyUntrustedCaller(requestId: string, data?: any, messageType?: string): void;
    /**
     * Loads and EXECUTES a remote script
     * The call is ASYNCHRONOUS
     *
     * @method LoadJsScript
     * @param {string} src The script URL
     * @param {Function} [clbkSuccess] Called when the script terminates execution
     * @param {Function} [clbkError] Called in case of error
     */
    function LoadJsScript(src: string, clbkSuccess?: (data: any, textStatus: string, jqXHR: JQueryXHR) => any, clbkError?: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => any): any;
    /**
     * Sets a cookie (updates it if already present)
     *
     * @method SetCookie
     * @param name The cookie name
     * @param value The cookie value
     * @param days IF passed specifies the number of days this coolie will be valid, otherwise FOREVER
     * @param path IF passed specifies the path of validity of the cookie (defaults to "/")
     * @param domain IF passed specifies the domain of validity of the cookie
     */
    function SetCookie(name: string, value: string, days?: number, path?: string, domain?: string): void;
    /**
     * Makes the specified cookie INVALID
     *
     * @method DeleteCookie
     */
    function DeleteCookie(name: string): void;
    /**
     * Returns the specified cookie value, or NULL if not found
     *
     * @method GetCookie
     */
    function GetCookie(name: string): string;
    /**
     * Downloads a resource using AJAX and shows the "Save As..." dialog
     * Supports any HTTP method and headers.
     * If the server returns the "content-disposition" header it will be used for the default filename.
     *
     * WARNING: in order to make it work with different domains (CORS) the server
     *          MUST return the header "Access-Control-Expose-Headers: content-disposition"
     *          (In dotnet CORE configure this behavior with .WithExposedHeaders(new string[]{"content-disposition"})
     *
     * @param {string} url The url to call
     * @param {string} method The http method to use
     * @param {string} defaultFilename The default filename if "content-disposition" header is not returned
     * @param {object} headers Eventually a dictionary holding headers to send
     * @param requestContentType Eventually the request content type (ex:  "application/x-www-form-urlencoded")
     * @param bodyData  Eventually the request data body
     * @param onError Eventually a callback called upon request ERROR
     * @param onSuccess Eventually a callback called upon request SUCCESS
     */
    function DownloadFileWithAjax(url: string, method?: string, defaultFilename?: string, headers?: object, requestContentType?: string, bodyData?: string, onError?: (request: any) => void, onSuccess?: (data: any) => void): void;
    /**
     * Namespace holding WRAPPERS methods to IronLibsNet.Ajax() call defaulting the HTTP METHOD to the corresponding REST action.
     * All methods receive the same IAjaxSettings supported by IronLibsNet.Ajax()
     *
     * @class IronLibsNet.REST
     */
    module REST {
        /**
         * Performs a GET
         *
         * @method Read
         */
        function Read(opt: IAjaxSettings): JQueryXHR;
        /**
         * Performs a POST
         *
         * @method Create
         */
        function Create(opt: any): JQueryXHR;
        /**
         * Performs a PUT
         *
         * @method Update
         */
        function Update(opt: any): JQueryXHR;
        /**
         * Performs a DELETE
         *
         * @method Delete
         */
        function Delete(opt: any): JQueryXHR;
    }
}
