"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsNet = void 0;
window["require"] = function () { return { "IronLibsCommon": window["IronLibsCommon"] }; }; //module import hack
const IronLibs = require("ironlibs-base");
let __ = IronLibs.IronLibsCommon;
window["require"] = function () { return { "IronLibsUi": window["IronLibsUi"] }; }; //module import hack
const IronLibsUi = require("./ui");
let __UI = IronLibsUi.IronLibsUi;
/**
 * CommonLib library dealing with networking
 * (runs in browser, requires JQuery library)
 */
var IronLibsNet;
(function (IronLibsNet) {
    /**
     * Returns TRUE if url starts with a common networking protocol
     *
     * @method IsAbsoluteUrl
     * @param {string} url
     * @returns {boolean}
     */
    function IsAbsoluteUrl(url) {
        url = __.EnsureString(url).trim().toLowerCase();
        return __.IsNotEmptyString(url) && ((url.indexOf("http://") == 0 && url.length > "http://".length) ||
            (url.indexOf("https://") == 0 && url.length > "https://".length) ||
            (url.indexOf("ftp://") == 0 && url.length > "ftp://".length) ||
            (url.indexOf("ftps://") == 0 && url.length > "ftps://".length) ||
            (url.indexOf("ws://") == 0 && url.length > "ws://".length) ||
            (url.indexOf("wss://") == 0 && url.length > "wss://".length));
    }
    IronLibsNet.IsAbsoluteUrl = IsAbsoluteUrl;
    /**
     * Considers the passed string a URL.
     * Returns the relative host.
     * If the url is RELATIVE returns the CURRENT host
     */
    function GetUrlDomain(url) {
        url = __.EnsureString(url).trim();
        let posDouble = url.indexOf("//");
        let withProtocol = false;
        if (posDouble >= 0) {
            url = url.substr(posDouble + 2);
            withProtocol = true;
        }
        let posSingle = url.indexOf("/");
        if (posSingle < 0) //no other slash? if url had the protocol what remains is the HOST, otherwise consider a RELATIVE url
            return withProtocol ? url : location.host;
        else if (posSingle == 0) //RELATIVE url
            return location.host;
        else
            return url.substr(0, posSingle);
    }
    IronLibsNet.GetUrlDomain = GetUrlDomain;
    /**
     * Returns the (absolute or relative) url WITHOUT DOUBLE SLASHES.
     * Doesn't check the last character (can end WITH or WITHOUT '/')
     *
     * @method SanitizeUrl
     * @param url the url to sanitize
     */
    function SanitizeUrl(url) {
        url = __.EnsureString(url).trim();
        if (IsAbsoluteUrl(url)) {
            let protocolIdx = url.indexOf("//") + 2;
            return url.substr(0, protocolIdx) + url.substr(protocolIdx).replace(/\/\//gi, "\/");
        }
        else
            return url.replace(/\/\//gi, "\/");
    }
    IronLibsNet.SanitizeUrl = SanitizeUrl;
    /**
     * Decodes all components of a query string to a plain object.
     * Supports multiple occurrences of the same parameter (as arrays values)
     *
     * @method UrlToObject
     * @param url (not mandatory) the part of the url containing the encoded parameters (defaults to window.location.search.substring(1) )
     * @return the parameters MAP
     */
    function UrlToObject(url) {
        let urlParams = {};
        let e, a = /\+/g, // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g, d = function (s) { return decodeURIComponent(s.replace(a, " ")); }, q = __.IsNull(url) || typeof url != "string" ? window.location.search.substring(1) : url;
        while (e = r.exec(q)) {
            let key = d(e[1]);
            let value = d(e[2]);
            if (__.IsNull(urlParams[key]))
                urlParams[key] = value;
            else if (__.IsArray(urlParams[key]))
                urlParams[key].push(value);
            else if (__.IsString(urlParams[key])) {
                urlParams[key] = [urlParams[key]];
                urlParams[key].push(value);
            }
        }
        return urlParams;
    }
    IronLibsNet.UrlToObject = UrlToObject;
    /**
     * Converts all object properties to an encoded query string
     * (Does the opposite of UrlToObject)
     * Supports multiple occurrences of the same parameter (as arrays values)
     *
     * @method ObjectToUrl
     */
    function ObjectToUrl(obj) {
        let url = "";
        for (let k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            if (__.IsArray(obj[k])) {
                (obj[k]).forEach((kk) => {
                    url += (url == "" ? "?" : "&") + encodeURIComponent(k) + "=" + encodeURIComponent(__.EnsureString(kk));
                });
            }
            else
                url += (url == "" ? "?" : "&") + encodeURIComponent(k) + "=" + encodeURIComponent(__.EnsureString(obj[k]));
        }
        return url;
    }
    IronLibsNet.ObjectToUrl = ObjectToUrl;
    /**
     * Checks the "Notifies" and shows messages to the user according to their type
     *
     * @method ShowNotifiesInModel
     * @param model Can be an XHR response or the model returned from an AjaxCall
     * @param $parentForPropertyErrors?
     * @param invalidateInputField?
     */
    function ShowNotifiesInModel(model, $parentForPropertyErrors, invalidateInputField) {
        let UI = __UI;
        if (__.IsNull(UI))
            return;
        let showErrMsg = true;
        if (__.IsNotNullObject(model) && __.IsArray(model.Notifications)) {
            model.Notifications.forEach(function (n) {
                if (__.IsNotEmptyString(n.PropertyName)) {
                    if (!__.IsJquery($parentForPropertyErrors))
                        $parentForPropertyErrors = $("body");
                    let $el = $parentForPropertyErrors.find("[name='" + n.PropertyName + "']");
                    if ($el.length && __.IsFunction(invalidateInputField)) {
                        invalidateInputField($el, n.PropertyName, n.MessageText);
                    }
                    else {
                        UI.NotificateErrors(n.MessageText, n.PropertyName); //since we have a PropertyName ALWAYS SHOW the MessageText
                    }
                }
                else {
                    if (n.Type == 0)
                        UI.NotificateInfo(n.MessageText);
                    else
                        UI.NotificateErrors(showErrMsg ? n.MessageText : App.Res.ErrorDuringWork);
                }
            });
        }
    }
    IronLibsNet.ShowNotifiesInModel = ShowNotifiesInModel;
    IronLibsNet.LogoffUrl = "/Account/Logoff";
    /**
     * The default Ajax options
     *
     * @static
     * @property {IAjaxSettings} AjaxDefaultOptions
     */
    IronLibsNet.AjaxDefaultOptions = {
        type: 'POST',
        dataType: 'json',
        async: true,
        contentType: 'application/json; charset=utf-8',
        threatInvalidDataInSuccess: false,
        headers: {
            "Language": __.GetCurrentLanguage()
        },
        /**
         * This is used to LIMIT the eventual search for the INPUT field notified for an error (NotificationModel.PropertyName)
         */
        $parentForPropertyErrors: $("body"),
        /**
         * This is used to render the eventual error received from server for a (found) input element
         * @param $input
         * @param propertyName
         * @param message
         */
        invalidateInputField: function ($input, propertyName, message) {
            $input.toggleClass("invalid", true);
            $('<em></em>')
                .attr("for", propertyName)
                .addClass("invalid")
                .text(message)
                .insertAfter($input);
        },
        error: function (e) {
            if (__.IsNotNull(e.responseJSON) && __.IsArray(e.responseJSON.Notifications)) {
                IronLibsNet.ShowNotifiesInModel(e.responseJSON, this.$parentForPropertyErrors, this.invalidateInputField);
            }
            else {
                if (__.IsNotNull(__UI)) //could not be imported
                    __UI.NotificateErrors(App.Res.ErrorDuringWork);
            }
        },
        complete: function () {
            if (__.IsNotNull(__UI)) //could not be imported
                __UI.UnblockPage();
        },
        authorizationError: function () {
            __UI.UnblockAllElements();
            setTimeout(function () {
                __UI.Modal.ShowDialog({
                    content: App.Res.AuthenticationProblem,
                    clbkOnClose: function () {
                        document.location.href = IronLibsNet.LogoffUrl;
                    },
                    showHeader: false,
                    showFooter: false,
                    closeOnOverlayClick: true,
                    closeOnEsc: true
                });
            }, 600);
        }
    };
    /***
     * Make an AJAX call
     *
     * @method Ajax
     * @param  {IAjaxSettings} option options to configure the call
     */
    function Ajax(option) {
        //get some Blox.Common.Client.Web.Base if are present....
        if (__.IsNull($))
            throw new Error("IronLibsNet.Ajax needs JQuery");
        let settings = __.MergeObj(__.CloneObj(IronLibsNet.AjaxDefaultOptions), option || {});
        let originalSuccess = settings.success;
        let originalError = settings.error;
        //F.P. convert OBJECTS to JSON
        if (__.IsNotNullObject(settings.data))
            settings.data = JSON.stringify(settings.data);
        //F.P. Async handling for Selenium tests
        //let asyncId = null;
        //if (__.IsNotNull(SeleniumHelper) && __.IsNotNull(SeleniumHelper.GLB.LastAjaxCallId))
        //{
        //    asyncId = SeleniumHelper.GLB.LastAjaxCallId; //remember the id to update its status
        //    SeleniumHelper.GLB.LastAjaxCallId = null; //permit to wait for other async JsonCalls
        //}
        settings.error = function (xhr, b, c) {
            //CHECK FOR UNAUTHORIZED CALL
            if (xhr.status == 401 && __.IsFunction(settings.authorizationError)) {
                settings.authorizationError(xhr, b, c);
                return;
            }
            //generic error
            //use eventual passed callback
            if (__.IsFunction(originalError)) {
                originalError(xhr, b, c);
                return;
            }
        };
        settings.success = function (data, state, xhr) {
            function RunSuccess() {
                //F.P. This is needed for Selenium tests to be aware of the async operation termination
                //if (__.IsNotNull(asyncId))
                //{
                //    SeleniumHelper.SetAsyncStatus(asyncId, "success");
                //}
                if (__.IsFunction(originalSuccess)) {
                    originalSuccess(data, state, xhr);
                }
            }
            //Check if received model contains error state
            if (__.IsNull(data) || typeof data !== 'object' || __.IsNull(data.IsValid)) {
                RunSuccess();
                return;
            }
            //became false when there are notifies with InvalidData || Error
            if (data.IsValid === false) {
                if (settings.threatInvalidDataInSuccess) {
                    //user doesn't want to consider ErrorNotifications as an Error result of the call
                    RunSuccess();
                }
                else if (__.IsFunction(settings.error)) {
                    settings.error(xhr, "", "");
                }
            }
            else {
                //if there are notifications in model, SHOW them!
                IronLibsNet.ShowNotifiesInModel(xhr, settings.$parentForPropertyErrors, settings.invalidateInputField);
                RunSuccess();
            }
        };
        //remove double slashes
        settings.url = IronLibsNet.SanitizeUrl(settings.url);
        return $.ajax(settings);
    }
    IronLibsNet.Ajax = Ajax;
    /**
     * Makes a NON-AJAX call, so can reach different and UNTRUSTED domains (no CORS blocks).
     * PERMITS to be notified upon SUCCESS or ERROR, along with the (eventually JSON parsed) response.
     * LIMITATION: doesn't SEND (eventually already present) and CREATE (eventually received new) cookies for the url domain
     *
     * @param url The url (untrusted) to be called
     * @param data Eventual data to be sent
     * @param onEnd callback receiving an ERROR or the DATA upon success
     * @param parseAsJSON If TRUE the received data is parsed to JSON, otherwise it's passed to callback as-is
     */
    function CallUntrustedWithoutCookies(url, data = null, onEnd = null, parseAsJSON = true) {
        if (!__.IsFunction(onEnd))
            onEnd = () => { };
        setTimeout(() => {
            $("<div></div>").load(url, data, function (data, status) {
                if (status == "success") {
                    if (parseAsJSON == false)
                        onEnd(null, data);
                    else {
                        try {
                            let dataJson = JSON.parse(__.EnsureString(data).trim());
                            onEnd(null, dataJson);
                        }
                        catch (e) {
                            onEnd(new Error(e.message), null);
                        }
                    }
                }
                else {
                    onEnd(new Error(status), null);
                }
            });
        }, 10);
    }
    IronLibsNet.CallUntrustedWithoutCookies = CallUntrustedWithoutCookies;
    /**
     * Makes a NON-AJAX call, so can reach different and UNTRUSTED domains (no CORS blocks).
     * PERMITS to be notified upon SUCCESS or ERROR, along with the (eventually JSON parsed) response.
     * PROS: Differently from "CallUntrustedWithoutCookies" the call SENDS (eventually already present) and CREATES (eventually received new) cookies for the url domain.
     * LIMITATION 1: The called resource MUST return an HTML page with a SCRIPT notifying the caller about the successful response
     *               (so the callee must be AWARE of the particular response to give)
     * LIMITATION 2: The called resource MUST accept a "requestId" parameter to be passed back to the caller
     * LIMITATION 3: The only ERROR type returned is TIMEOUT: we can't detect the real reason of the failure (StatusCode),
     *               simply the caller will never be notified about the success, so to detect the error the timeout is the only way...
     *
     * Example code of the callee to be responded:
     * if (parent != null && parent != window)
     * {
     *   parent.postMessage(JSON.stringify(
     *       {
     *           messageType: "PageLoaded",
     *           requestId: "@Html.Raw((ViewBag.RequestId as string).Replace("\"", "\\\""))",
     *           otherData : {a: 11, b: 22}
     *       }), "*");
     * }
     *
     * @param url The url (untrusted) to be called
     * @param onEnd callback receiving an ERROR or the DATA upon success
     * @param timeoutMs
     */
    function CallUntrustedWithCookies(url, onEnd = null, timeoutMs = 5000) {
        InitCallUntrustedWithCookies();
        let requestId = __.Guid.New();
        if (url.indexOf("?") >= 0)
            url += "&requestId=" + requestId;
        else
            url += "?requestId=" + requestId;
        if (!__.IsFunction(onEnd))
            onEnd = () => { };
        untrustedCalls[requestId] = {
            callback: onEnd,
            receivedResponse: false,
            timeout: false
        };
        //check for timeout
        setTimeout(function () {
            if (!untrustedCalls[requestId].receivedResponse) {
                untrustedCalls[requestId].timeout = true;
                onEnd(new Error("Timeout for call to '" + url + "'"), null);
            }
        }, timeoutMs);
        $("<iframe></iframe>").attr("src", url)
            .hide()
            .appendTo("body");
    }
    IronLibsNet.CallUntrustedWithCookies = CallUntrustedWithCookies;
    let callUntrustedWithCookiesInitialized = false;
    let untrustedCalls = {};
    function InitCallUntrustedWithCookies() {
        if (callUntrustedWithCookiesInitialized)
            return;
        callUntrustedWithCookiesInitialized = true;
        window.addEventListener('message', function (event) {
            try {
                let obj = JSON.parse(event.data);
                if (__.IsNotNull(obj)) {
                    //this message comes from ECOMMERCE
                    //search and call the callback relative to this message (only ONCE)
                    for (let r in untrustedCalls) {
                        if (!untrustedCalls.hasOwnProperty(r))
                            continue;
                        if (r == obj.requestId) {
                            if (untrustedCalls[r].timeout == false && untrustedCalls[r].receivedResponse == false) {
                                untrustedCalls[r].receivedResponse = true;
                                untrustedCalls[r].callback(null, obj);
                            }
                        }
                    }
                }
            }
            catch (e) {
                console.log("ERROR: cant handle 'message' from '" + event.origin + "': " + event.data);
            }
        });
    }
    /**
     * To be used in response to a call performed with "CallUntrustedWithCookies"
     *
     * @param requestId The same requestId passed to the called url
     * @param data An eventual data to pass as response
     * @param messageType An eventual message type
     */
    function NotifyUntrustedCaller(requestId, data = null, messageType = "PageLoaded") {
        if (parent != null && parent != window) //execute only when called inside an IFRAME
         {
            parent.postMessage(JSON.stringify({
                messageType: messageType,
                requestId: requestId,
                data: data
            }), "*");
        }
    }
    IronLibsNet.NotifyUntrustedCaller = NotifyUntrustedCaller;
    /**
     * Loads and EXECUTES a remote script
     * The call is ASYNCHRONOUS
     *
     * @method LoadJsScript
     * @param {string} src The script URL
     * @param {Function} [clbkSuccess] Called when the script terminates execution
     * @param {Function} [clbkError] Called in case of error
     */
    function LoadJsScript(src, clbkSuccess, clbkError) {
        /*
         //This method IS NOT SAFE for Opera and should be tested some more...

         let s = document.createElement('script');
         s.type = 'text/' + (src.type || 'javascript');
         s.src = src.src || src;
         s.async = false;

         s.onreadystatechange = s.onload = function ()
         {

         let state = s.readyState;

         if (!callback.done && (!state || /loaded|complete/.test(state)))
         {
         callback.done = true;
         callback(src);
         }
         };

         // use body if available. more safe in IE
         (document.body || head).appendChild(s);
         */
        let opt = {
            url: src,
            type: "GET",
            dataType: "script",
            cache: true,
            success: clbkSuccess,
            error: clbkError
        };
        return IronLibsNet.Ajax(opt);
    }
    IronLibsNet.LoadJsScript = LoadJsScript;
    /**
     * Sets a cookie (updates it if already present)
     *
     * @method SetCookie
     * @param name The cookie name
     * @param value The cookie value
     * @param days IF passed specifies the number of days this coolie will be valid, otherwise FOREVER
     * @param path IF passed specifies the path of validity of the cookie (defaults to "/")
     * @param domain IF passed specifies the domain of validity of the cookie
     */
    function SetCookie(name, value, days, path, domain) {
        let opt = {
            path: "/"
        };
        if (__.IsNotEmptyString(path))
            opt.path = path;
        if (__.IsNotEmptyString(domain))
            opt.domain = domain;
        if (__.IsNumber(days)) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            opt.expires = date; //"; expires=" + date.toGMTString();
        }
        $.cookie(name, value, opt);
        /*
         let expires = "";
         if (__.IsNumber(days))
         {
         let date = new window["Date"]();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         expires = "; expires=" + date.toGMTString();
         }

         document.cookie = name + "=" + value + expires + "; path=/";
         */
    }
    IronLibsNet.SetCookie = SetCookie;
    /**
     * Makes the specified cookie INVALID
     *
     * @method DeleteCookie
     */
    function DeleteCookie(name) {
        SetCookie(name, "", -1);
    }
    IronLibsNet.DeleteCookie = DeleteCookie;
    /**
     * Returns the specified cookie value, or NULL if not found
     *
     * @method GetCookie
     */
    function GetCookie(name) {
        return $.cookie(name);
        /*
         let nameEQ = name + "=";
         let ca = document.cookie.split(';');
         for (let i = 0; i < ca.length; i++)
         {
         let c = ca[i];
         while (c.charAt(0) == ' ')
         c = c.substring(1, c.length);

         if (c.indexOf(nameEQ) == 0)
         return c.substring(nameEQ.length, c.length);
         }
         return null;
         */
    }
    IronLibsNet.GetCookie = GetCookie;
    /**
     * Downloads a resource using AJAX and shows the "Save As..." dialog
     * Supports any HTTP method and headers.
     * If the server returns the "content-disposition" header it will be used for the default filename.
     *
     * WARNING: in order to make it work with different domains (CORS) the server
     *          MUST return the header "Access-Control-Expose-Headers: content-disposition"
     *          (In dotnet CORE configure this behavior with .WithExposedHeaders(new string[]{"content-disposition"})
     *
     * @param {string} url The url to call
     * @param {string} method The http method to use
     * @param {string} defaultFilename The default filename if "content-disposition" header is not returned
     * @param {object} headers Eventually a dictionary holding headers to send
     * @param requestContentType Eventually the request content type (ex:  "application/x-www-form-urlencoded")
     * @param bodyData  Eventually the request data body
     * @param onError Eventually a callback called upon request ERROR
     * @param onSuccess Eventually a callback called upon request SUCCESS
     */
    function DownloadFileWithAjax(url, method = "GET", defaultFilename = "download", headers, requestContentType, bodyData, onError, onSuccess) {
        let req = new XMLHttpRequest();
        req.open(method, url, true);
        req.responseType = "blob";
        if (!__.IsNotNullObject(headers))
            headers = {};
        for (let headerKey in headers) {
            if (!headers.hasOwnProperty(headerKey))
                continue;
            req.setRequestHeader(headerKey, headers[headerKey]);
        }
        if (__.IsNotEmptyString(requestContentType))
            req.setRequestHeader("Content-type", requestContentType);
        req.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status >= 200 && this.status < 300) {
                    let blob = null;
                    if (this.response instanceof Blob)
                        blob = this.response;
                    else if (this.response.json instanceof Blob)
                        blob = this.response.json;
                    let filenameHeader = this.getResponseHeader("content-disposition"); //if you have the fileName header available
                    if (__.IsNotEmptyString(filenameHeader) && filenameHeader.indexOf("filename=") >= 0) {
                        defaultFilename = filenameHeader.substr(filenameHeader.indexOf("filename=") + 9);
                        defaultFilename = decodeURIComponent(defaultFilename); //in Blox.Drive we URL-ENCODE the filename (to avoid non-ASCII charachter exception in returned headers)
                    }
                    let link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = defaultFilename;
                    link.style.display = "none";
                    $("body").append(link);
                    link.click();
                    if (__.IsFunction(onSuccess))
                        onSuccess(this.response);
                }
                else {
                    if (__.IsFunction(onError))
                        onError(this);
                }
            }
        };
        req.send(bodyData);
    }
    IronLibsNet.DownloadFileWithAjax = DownloadFileWithAjax;
    /**
     * Namespace holding WRAPPERS methods to IronLibsNet.Ajax() call defaulting the HTTP METHOD to the corresponding REST action.
     * All methods receive the same IAjaxSettings supported by IronLibsNet.Ajax()
     *
     * @class IronLibsNet.REST
     */
    let REST;
    (function (REST) {
        /**
         * Performs a GET
         *
         * @method Read
         */
        function Read(opt) {
            let _opt = __.MergeObj(__.IsNull(opt) ? {} : opt, { "type": 'GET' });
            return IronLibsNet.Ajax(_opt);
        }
        REST.Read = Read;
        /**
         * Performs a POST
         *
         * @method Create
         */
        function Create(opt) {
            let _opt = __.MergeObj(__.IsNull(opt) ? {} : opt, { "type": 'POST' });
            return IronLibsNet.Ajax(_opt);
        }
        REST.Create = Create;
        /**
         * Performs a PUT
         *
         * @method Update
         */
        function Update(opt) {
            let _opt = __.MergeObj(__.IsNull(opt) ? {} : opt, { "type": 'PUT' });
            return IronLibsNet.Ajax(_opt);
        }
        REST.Update = Update;
        /**
         * Performs a DELETE
         *
         * @method Delete
         */
        function Delete(opt) {
            let _opt = __.MergeObj(__.IsNull(opt) ? {} : opt, { "type": 'DELETE' });
            return IronLibsNet.Ajax(_opt);
        }
        REST.Delete = Delete;
    })(REST = IronLibsNet.REST || (IronLibsNet.REST = {}));
})(IronLibsNet = exports.IronLibsNet || (exports.IronLibsNet = {}));
//# sourceMappingURL=net.js.map