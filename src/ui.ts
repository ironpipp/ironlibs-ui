﻿
declare let window : Window;

(<any>window)["require"] = function() {return {"IronLibsCommon" : window["IronLibsCommon"]}; };    //module import hack
import IronLibs = require('ironlibs-base');

let __ = IronLibs.IronLibsCommon;

(<any>window)["require"] = function() {return {"IronLibsStore" : window["IronLibsStore"]}; };    //module import hack
import IronLibsStore = require('./store');

let __Store = IronLibsStore.IronLibsStore;


/**
 * CommonLib library dealing with DOM and HTML
 * (runs in browser, requires JQuery library)
 */
export module IronLibsUi
{
    let isNode = window == null;
    
    /*##########################################################################
     ######################   GENERAL UI RELATED METHODS   #####################
     ##########################################################################*/


    /**
     * Returns a string with escaped HTML characters
     *
     * @method EscapeHtml
     * @param html {string} The plain string to escape
     * @return {string} The string with HTML ESCAPES
     */
    export function EscapeHtml(html : string) : string
    {
        return $("<span></span>").text(html).html();
    }


    /**
     * Returns a string with unescaped HTML characters
     *
     * @method UnescapeHtml
     * @param html {string} A string with HTML ESCAPES
     * @return {string} The decoded string
     */
    export function UnescapeHtml(html : string) : string
    {
        return $("<span></span>").html(html).text();
    }



    /**
     * If the tag's value is NOT EMPTY and is NOT an URL then prepends urlPrefix (defaults to "http://") to it
     *
     * @method EnsureInputValueIsUrl
     * @param $inputTag {JQuery} a JQuery wrapped INPUT TAG
     * @param [urlPrefix] {string} the string to prepend to the input value if it's not an url
     */
    export function EnsureInputValueIsUrl($inputTag : JQuery, urlPrefix : string = "http://") : void
    {
        let s = (<string>$inputTag.val()).trim();
        if (!__.StringIsUrl(s))
        {
            $inputTag.val(urlPrefix + s);
        }
    }



    /**
     * Returns TRUE if the DOM document is actually exposed to the user
     * (the browser tab is active)
     *
     * @method IsDocumentVisible
     * @return boolean
     */
    export function IsDocumentVisible() : boolean
    {
        let hidden = document.visibilityState == "hidden" ||
            document["webkitVisibilityState"] == "hidden" ||
            document["webkitVisibilityState"] == "unloaded" ||
            document["webkitVisibilityState"] == "not visible";

        return !hidden;
    }



    /**
     * If the current browser is Internet Explorer, returns its version, NULL otherwise
     *
     * @method InternetExplorerVersion
     * @return number | NULL
     */
    export function InternetExplorerVersion() : number
    {
        if (__.IsNull(navigator) || __.IsEmptyString(navigator.userAgent))
            return null;

        let ua = navigator.userAgent;
        if (ua.indexOf("Trident/7") >= 0) //special case for IE11
            return 11.0;
        let pos = ua.indexOf("MSIE ");
        if (pos < 0)
            return null;

        //it's IE: parse its version
        ua = ua.substr(pos + 5);
        pos = ua.indexOf(";");
        ua = ua.substr(0, pos);

        let ver = parseFloat(ua);
        if (isNaN(ver))
            return null;

        return ver;
    }


    /**
     * Checks if the page has been restored after a browser restart WITHOUT re-executing the request
     * to the server. This is useful to be sure that old scripts are ever executed!
     * WARNING: Requires an HIDDEN INPUT tag with id="requestId" with a random value at each rendering to work
     *
     * @method IsPageFresh
     * @return {boolean} TRUE if the page is fresh or the check can't be executed, FALSE if the page is proven to be OLD.
     */
    export function IsPageFresh() : boolean
    {
        if (window["SpaManager"] != undefined)
            return true; //for now SPA doesn't support this

        try
        {
            //without using Modernizr (NOT included in PUBLIC area) check if we have localStorage support
            try
            {
                __Store.Local.Set('modernizr', 'modernizr');
                __Store.Local.Remove('modernizr');
            }
            catch (e)
            {
                return true;
            }

            let MAX_REQUESTS_HISTORY = 100;
            if (__Store.Local.Get("requests") == undefined)
                __Store.Local.Set("requests", []);

            let currRequestId = $("#requestId").val();
            let requests = __Store.Local.Get("requests");

            //search the current ID
            for (let i = 0; i < requests.length; i++)
            {
                if (requests[i] == currRequestId)
                {
                    //The page has already been loaded with this ID, so it's OLD
                    return false;
                }
            }
            //the page is NEW, so remember its ID
            if (requests.length == MAX_REQUESTS_HISTORY)
                requests.splice(0, 1); //remove the oldest request

            requests.push(currRequestId);

            __Store.Local.Set("requests", requests);
            return true;
        }
        catch (e)
        {
            return true;
        }
    }



    /**
     * Returns a structure describing the browsing device.
     * The detection is performed by the CSS ENGINE, so reflects the MEDIA QUERIES rules of the different devices types
     *
     * @return {IsDesktop : boolean; IsTablet : boolean; IsPhone : boolean; IsHorizontal : boolean; IsVertical : boolean}
     * @method GetBrowsingDeviceType
     */
    export function GetBrowsingDeviceType() : {
        IsDesktop : boolean;
        IsTablet : boolean;
        IsPhone : boolean;
        IsHorizontal : boolean;
        IsVertical : boolean;
    }
    {
        let $bt = $("#BrowserType");
        let type = $bt.css("background-color").toLowerCase();
        let orientation = $bt.css("color").toLowerCase();

        return {
            IsDesktop : type.indexOf("#0000ff") >= 0 || type.indexOf("rgb(0, 0, 255)") >= 0,
            IsTablet  : type.indexOf("#00ff00") >= 0 || type.indexOf("rgb(0, 255, 0)") >= 0,
            IsPhone   : type.indexOf("#ff0000") >= 0 || type.indexOf("rgb(255, 0, 0)") >= 0,

            IsHorizontal : orientation.indexOf("#0000ff") >= 0 || orientation.indexOf("rgb(0, 0, 255)") >= 0,
            IsVertical   : orientation.indexOf("#00ff00") >= 0 || orientation.indexOf("rgb(0, 255, 0)") >= 0
        };
    }



    /**
     * Returns the wrapped Document object of the IFRAME found under the specified selector
     *
     * @method GetIframeInnerDocument
     */
    export function GetIframeInnerDocument($iframe : JQuery) : JQuery
    {
        if (__.IsNull($iframe) || $iframe.length == 0)
            return null;

        let cw = (<any>($iframe[0])).contentWindow;

        if (__.IsNotNull(cw))
            return $(cw.document);

        //Internet Explorer case
        let id = $iframe.attr("id");
        if (__.IsEmptyString(id))
        {
            id = __.Guid.New();
            $iframe.attr("id", id);
        }
        return $((<any>document).frames[id].document);
    }



    /**
     * Chrome browser on MOBILE devices (at the moment) when user over-scrolls up the page
     * a RELOAD PAGE button appears and activates when user releases the touch,
     * actually reloading the page.
     * This functions checks the BODY scroll situation and prevents this.
     *
     * @method PreventPullToRefresh
     */
    export function PreventPullToRefresh() : void
    {
        let lastScrollPosition = null;
        let $body = $("#content").find(" > div"); //the main page scrolling element

        $body.bind("touchstart", function(e)
        {
            if ((<any>e.originalEvent).changedTouches.length == 0)
                return;

            lastScrollPosition = (<any>e.originalEvent).changedTouches[0].clientY;
        });

        $body.bind("touchmove", function(e)
        {
            if ((<any>e.originalEvent).changedTouches.length == 0)
                return;

            let t = (<any>e.originalEvent).changedTouches[0];

            if ($body[0].scrollTop == 0) //if there's nothing more to scroll
            {
                if (t.clientY > lastScrollPosition) //...and user is scrolling UP
                {
                    e.preventDefault(); //THIS prevents the PullToRefresh browser feature
                }
                else
                {
                    lastScrollPosition = t.clientY;
                }
            }
        });
    }


    export function ShowOldBrowserMsg() : void
    {
        NotificateInfo($("#OldBrowserMsg").clone().show().wrap($d()).parent().html());
    }



    /**
     * Changes the TAG TYPE of the passed element keeping the attributes and data() values
     *
     * @method ChangeElementType
     * @param $el {JQuery} the elements to be changed
     * @param newType {string} the new tag name
     * @return the newly created elements
     */
    export function ChangeElementType($el : JQuery, newType : string) : JQuery
    {
        let $ret = $();

        $el.each(function()
        {
            let el = this;
            let attrs = {};
            let data = $el.data();

            $.each(el.attributes, function(idx, attr)
            {
                attrs[attr.nodeName] = attr.nodeValue;
            });

            $(el).replaceWith(function()
            {
                let $newEl = $("<" + newType + "/>", attrs)
                    .append($el.contents())
                    .data(data);

                $ret = $ret.add($newEl);
                return $newEl;
            });
        });

        return $ret;
    }


    /**
     * Copy ALL the style COMPUTED properties of nodeSource to nodeDest
     *
     * @method CloneNodeStyle
     * @param nodeSource {HTMLElement} a DOM node (not jQuery object)
     * @param nodeDest  {HTMLElement} a DOM node (not jQuery object)
     */
    export function CloneNodeStyle(nodeSource : HTMLElement, nodeDest : HTMLElement) : void
    {
        let sourceStyle = nodeSource.style;
        let jQuerySource = $(nodeSource);

        for (let s in sourceStyle)
        {
            if (typeof sourceStyle[s] != "string" || s == "cssText")
                continue;

            //get the COMPUTED value and SET IT
            nodeDest.style[s] = jQuerySource.css(s);
        }
    }



    /*##########################################################################
     #############################   RENDERERS   ###############################
     ##########################################################################*/


    /**
     * Generates pretty HTML rendering a boolean value (using Bootstrap badge)
     *
     * @method RenderBool
     * @param {boolean} boolValue
     * @return {string} the rendered HTML string
     */
    export function RenderBool(boolValue : boolean) : string
    {
        if (!!boolValue)
            return '<span class="badge BoolYes">' + App.Res.Yes + '</span>';
        else
            return '<span class="badge BoolNo">' + App.Res.No + '</span>';
    }


    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-ONLY formatted string
     *
     * @method RenderDate
     * @return {string} the rendered HTML string
     */
    export function RenderDate(date : any) : string
    {
        let dateObj;
        try
        {
            if (__.IsDate(date))
                dateObj = date;
            else if (__.Dates.IsISOFormatStringDate(date))
                dateObj = __.Dates.GetDateFromISO(date);
            else if (__.Dates.IsMsFormatStringDate(date))
                dateObj = __.Dates.GetDateFromMsFormat(date);
        }
        catch (e)
        {
        }

        if (!__.IsDate(dateObj))
            return "";

        return __.Dates.FormatDate(dateObj, true, true, true, false, false, false, false);
    }


    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-AND-TIME formatted string
     *
     * @method RenderDateTime
     * @return {string} the rendered HTML string
     */
    export function RenderDateTime(date : any) : string
    {
        let dateObj;
        try
        {
            if (__.IsDate(date))
                dateObj = date;
            else if (__.Dates.IsISOFormatStringDate(date))
                dateObj = __.Dates.GetDateFromISO(date);
            else if (__.Dates.IsMsFormatStringDate(date))
                dateObj = __.Dates.GetDateFromMsFormat(date);
        }
        catch (e)
        {
        }

        if (!__.IsDate(dateObj))
            return "";

        return __.Dates.FormatDate(dateObj, true, true, true, true, true, true, false);
    }



    /**
     * Renders a Bootstrap toggle initialized with the passed value
     *
     * @method RenderBoolSwitcher
     * @return {string} the rendered HTML string
     */
    export function RenderBoolSwitcher(boolValue : boolean,
                                       inputName : string = "",
                                       inputId : string   = "") : string
    {
        /*
         return '<span class="onoffswitch">' +
         '<input type="checkbox" class="onoffswitch-checkbox"' +
         (__.IsNotEmptyString(inputName) ? (' name="' + inputName + '"') : '' ) +
         (__.IsNotEmptyString(inputId) ? (' id="' + inputId + '"') : '' ) +
         ((!!boolValue) ? ' checked="checked"' : '' ) + '>' +
         '<label class="onoffswitch-label">' +
         '<span class="onoffswitch-inner" data-swchon-text="' + App.Res.Yes + '"' +
         ' data-swchoff-text="' + App.Res.No + '"></span>' +
         '<span class="onoffswitch-switch"></span></label></span>';
         *
         */
        /*        return '<input type="checkbox" ' +
         (__.IsNotEmptyString(inputName) ? (' name="' + inputName + '"') : '' ) +
         (__.IsNotEmptyString(inputId) ? (' id="' + inputId + '"') : '' ) +
         ((!!boolValue) ? ' checked="checked"' : '' ) + '>';
         */
        return `<div class="smart-form">
                        <label class="toggle">
                            <input type="checkbox" ` +
            ((!!boolValue) ? ` checked="checked"` : ``) +
            (__.IsNotEmptyString(inputName) ? (` name="` + inputName + `"`) : ``) +
            (__.IsNotEmptyString(inputId) ? (` id="` + inputId + `"`) : ``) +
            `>
                            <i data-swchoff-text="` +
            App.Res.No +
            `" data-swchon-text="` +
            App.Res.Yes +
            `"></i>
                         </label>
                     </div>`;
    }


    /**
     * Renders a spinner image tag
     *
     * @method RenderSpinner
     * @return {string} the rendered HTML string
     */
    export function RenderSpinner() : string
    {
        return '<img alt="' + App.Res.Loading + '" src="' + DefaultSpinnerImageUrl + '" />';
    }



    /**
     * If the passed element is contained in a JqueryUI opened dialog, closes it
     *
     * @method CloseContainingPopup
     * @param $el {JQuery} the element to be checked
     */
    export function CloseContainingPopup($el : JQuery | HTMLElement) : void
    {
        let obj : any = $($el).parents(".ui-dialog-content:eq(0)").data("uiDialog");
        if (__.IsNotNull(obj) && __.IsFunction(obj.close))
            obj.close();
    }

    function $_addTheRest(node : JQuery, options : string | string[] | any)
    {
        if (typeof options == "string")
            node.addClass(<string>options);
        else if (__.IsArray(options))
            node.addClass((<string[]>options).join(" "));
        else if (typeof options == "object")
            node.attr(options);
    }


    /**
     * Fast helper function which returns a new SPAN element
     *
     * @method $s
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the span
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $s(content? : string | JQuery[] | JQuery,
                       options1? : string | string[] | any,
                       options2? : string | string[] | any) : JQuery
    {
        let ret = $("<span></span>");

        if (typeof content == "string")
            ret.text(<string>content);
        else if (__.IsArray(content))
            for (let c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$s"] = $s; //shortcut


    /**
     * Fast helper function which returns a new DIV element
     *
     * @method $d
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $d(content? : string | JQuery[] | JQuery,
                       options1? : string | string[] | any,
                       options2? : string | string[] | any) : JQuery
    {
        let ret = $("<div></div>");

        if (typeof content == "string")
            ret.html(<string>content);
        else if (__.IsArray(content))
            for (let c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$d"] = $d; //shortcut


    /**
     * Fast helper function which returns a new IMG element
     *
     * @method $i
     * @param src The attribute SRC of the image
     * @param alt The attribute ALT of the image
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $i(src : string = "",
                       alt : string = "",
                       options1? : string | string[] | any,
                       options2? : string | string[] | any) : JQuery
    {
        let ret = $("<img src='' alt='' />").attr({"src" : src, "alt" : alt});

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);

        return ret;
    }

    if (!isNode)
        window["$i"] = $i; //shortcut


    /**
     * Fast helper function which returns a new A element
     *
     * @method $a
     * @param href The attribute HREF of the A tag
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $a(href : string = "javascript:void(0);",
                       content? : string | JQuery[] | JQuery,
                       options1? : string | string[] | any,
                       options2? : string | string[] | any) : JQuery
    {
        let ret = $("<a></a>").attr("href", href);

        if (typeof content == "string")
            ret.html(<string>content);
        else if (__.IsArray(content))
            for (let c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);

        return ret;
    }

    if (!isNode)
        window["$a"] = $a; //shortcut


    /**
     * Fast helper function which returns a new SELECT element
     *
     * @method $sel
     * @param values An array of STRING or SelectListItem
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $sel(values : string[] | { Text : string; Value : string; Selected? : boolean }[],
                         options1? : string | string[] | any,
                         options2? : string | string[] | any) : JQuery
    {
        let ret = $("<select></select>");

        if (__.IsArray(values))
        {
            for (let i = 0; i < values.length; i++)
            {
                let el = values[i];
                let castedEl = <{ Text : string; Value : string; Selected? : boolean }>values[i];

                if (typeof el == "string")
                {
                    ret.append($opt(<string>el, <string>el));
                }
                else if (typeof el == "object" &&
                    __.IsNotNull(castedEl.Text) &&
                    __.IsNotNull(castedEl.Value))
                {
                    ret.append($opt(castedEl.Value, castedEl.Text, castedEl.Selected == true));
                }
            }
        }

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$sel"] = $sel; //shortcut


    /**
     * Returns an <OPTION> element
     *
     * @method $opt
     */
    export function $opt(value : string = "", text : string = "", selected : boolean = false, disabled : boolean = false) : JQuery
    {
        let $ret = $("<option></option>").attr("value", __.EnsureString(value)).text(__.EnsureString(text));
        if (selected)
            $ret.attr("selected", "selected");
        if (disabled)
            $ret.attr("disabled", "disabled");
        return $ret;
    }

    if (!isNode)
        window["$opt"] = $opt; //shortcut



    /**
     * Returns a list of <OPTION> elements for an ARRAY of data
     *
     * @method $opts
     */
    export function $opts(items : any[],
                          propForValue : string | ((any) => string)     = "Value",
                          propForText : string | ((any) => string)      = "Text",
                          propForSelected : string | ((any) => boolean) = "Selected",
                          propForDisabled : string | ((any) => boolean) = "Disabled") : JQuery
    {
        let $ret = $();
        if (__.IsArray(items))
            items.forEach((item : any) =>
            {
                let v = __.IsFunction(propForValue) ? (<((any) => string)>propForValue)(item) : __.EnsureString(__.GetObjProperty(item, <string>propForValue));
                let t = __.IsFunction(propForText) ? (<((any) => string)>propForText)(item) : __.EnsureString(__.GetObjProperty(item, <string>propForText));
                let s = __.IsFunction(propForSelected) ? (<((any) => boolean)>propForSelected)(item) : !!__.EnsureString(__.GetObjProperty(item, <string>propForSelected));
                let d = __.IsFunction(propForDisabled) ? (<((any) => boolean)>propForDisabled)(item) : !!__.EnsureString(__.GetObjProperty(item, <string>propForDisabled));

                $ret = $ret.add($opt(v, t, s, d));
            });
        return $ret;
    }

    if (!isNode)
        window["$opts"] = $opts; //shortcut



    /**
     * Fast helper function which returns a new TD element
     *
     * @method $td
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the TD node
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $td(content : string | JQuery[] | JQuery,
                        options1? : string | string[] | any,
                        options2? : string | string[] | any) : JQuery
    {
        let ret = $("<td></td>");

        if (typeof content == "string")
            ret.html(<string>content);
        else if (__.IsArray(content))
            for (let c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$td"] = $td; //shortcut


    /**
     * Fast helper function which returns a new TR element
     *
     * @method $tr
     * @param [values] A simple array of object(STRING or NODE or an ARRAY of nodes) representing the content
     *                 of the respective TD subnodes to be inserted.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $tr(values : any[],
                        options1? : string | string[] | any,
                        options2? : string | string[] | any) : JQuery
    {
        let ret = $("<tr></tr>");

        if (__.IsArray(values))
        {
            for (let i = 0; i < values.length; i++)
                ret.append($td(values[i]));
        }

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);

        return ret;
    }

    if (!isNode)
        window["$tr"] = $tr; //shortcut


    /**
     * Fast helper function which returns a new TABLE element
     *
     * @method $t
     * @param [content] If an array of array of object(STRING or NODE or an ARRAY of nodes) inserts all TR and TD subnodes.
     *                  If a simple array inserts only empty TR subnodes.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $t(content : any[],
                       options1? : string | string[] | any,
                       options2? : string | string[] | any) : JQuery
    {
        let tb = $("<tbody></tbody>");
        let ret = $("<table></table>").append(tb);

        if (__.IsArray(content))
        {
            for (let c = 0; c < content.length; c++)
                tb.append($tr(content[c]));
        }

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$t"] = $t; //shortcut


    /**
     * Fast helper function which returns a new STYLE element
     * Useful to COMPUTE the CSS and add/update it in the page dynamically
     *
     * @method $style
     * @param [css] The text containing the CSS rules
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    export function $style(css : string,
                           options1? : string | string[] | any,
                           options2? : string | string[] | any) : JQuery
    {
        let ret = $("<style></style>").html(css);

        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }

    if (!isNode)
        window["$style"] = $style; //shortcut


    /**
     * Forces the browser to load (and cache!) the passed image.
     * Callbacks are called upon success or error.
     *
     * @param {string} url
     * @param {(width: string, height: string, time: number, img: HTMLImageElement) => void} callback
     * @param {(time: number, img: HTMLImageElement) => void} callbackError
     * @param {boolean} disableCache
     * @method PreLoadImageWithClbk
     */
    export function PreLoadImageWithClbk(url : string,
                                         callback? : (width : string, height : string, time : number, img : HTMLImageElement) => void,
                                         callbackError? : (time : number, img : HTMLImageElement) => void,
                                         disableCache? : boolean) : void
    {
        if (disableCache)
        {
            url += (url.indexOf("?") < 0) ? "?" : "&";
            url += "fresh=" + (Math.random() * 10000000 | 0);
        }

        let start = new Date().getTime();
        let $i = IronLibsUi.$i(url);
        let $d = IronLibsUi.$d().appendTo("body").css({
            "position"   : "absolute",
            "left"       : "-100000px",
            "overflow"   : "hidden",
            "visibility" : "hidden"
        });

        $i.appendTo($d).bind("load", function()
            {
                let w = $i.css("width");
                let h = $i.css("height");
                let end = (new Date().getTime() - start);
                $d.remove();
                if (typeof callback == "function")
                    callback(w, h, end, <HTMLImageElement>$i.detach()[0]);
            })
            .bind("error", function(e)
            {
                let end = (new Date().getTime() - start);
                if (typeof callbackError == "function")
                    callbackError(end, <HTMLImageElement>$i.detach()[0]);
            });

        //IE issue FIX (normally if image is already downloaded the callback won't be called!)
        if ((<any>$i[0]).readyState == "complete") //.readyState === 4 ??
            callback($i.css("width"), $i.css("height"), (new Date().getTime() - start), <HTMLImageElement>$i.detach()[0]);
    }



    /**
     * Executes the passed JS script
     *
     * @param script {string}
     * @param $appendScriptTagTo {JQuery}
     * @method ExecuteJsScript
     */
    export function ExecuteJsScript(script : string, $appendScriptTagTo : JQuery = $("head")) : void
    {
        //create a NEW tag
        let s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = false;

        try
        {
            // doesn't work on ie...
            s.appendChild(document.createTextNode(script));
        }
        catch (e)
        {
            // IE has funky script nodes
            s.text = script;
        }

        $appendScriptTagTo[0].appendChild(s);
    }



    /**
     * Loads from network end EXECUTES in page a SCRIPT loading the specified url
     * When loading and execution are done, calls the CALLBACK if specified
     *
     * @method LoadJsLink
     * @param src Can be an URL or a NOT already inserted HTMLScriptElement tag
     * @param async
     * @param callback
     * @param appendTagTo
     * @return {JQuery} The created and inserted tag
     */
    export function LoadJsLink(src : string | HTMLScriptElement,
                               async : boolean,
                               callback? : (HTMLScriptElement) => void,
                               appendTagTo? : JQuery) : void
    {
        let url : string = (<HTMLScriptElement>src).src || <string>src;

        if (async)
        {
            $.ajax({
                url      : url,
                type     : "get",
                dataType : "script",
                data     : null,
                cache    : true,
                success  : callback
            });
        }
        else
        {
            //F.P. WARNING: the order (append tag, onload, set src) IS IMPORTANT for the callback to be called!
            let s = <HTMLScriptElement>(document.createElement('script'));
            let $s = $(s);
            s.type = 'text/' + ((<HTMLScriptElement>src).type || 'javascript');
            s.async = false;

            // use body if available. more safe in IE
            if (__.IsJquery(appendTagTo) && appendTagTo.length > 0)
                appendTagTo[0].appendChild(s);
            else
                (document.body || document.head).appendChild(s);

            if (__.IsFunction(callback))
            {
                (<any>s).onreadystatechange = s.onload = function()
                {
                    let state = (<any>s).readyState;

                    if ($s.data("OnLoadScriptDone") != true && (!state || /loaded|complete/.test(state)))
                    {
                        $s.data("OnLoadScriptDone", true); //remember it!
                        callback($s);
                    }
                };
            }

            s.src = url;
        }
    }



    /**
     * Loads end APPLIES in page a LINK (css) tag loading the specified url
     * When loading and apply are done, calls the CALLBACK if specified
     *
     * @method LoadCssLink
     * @param src Can be an URL or a NOT already inserted HTMLLinkElement tag
     * @param callback
     * @return {JQuery} The created and inserted tag
     */
    export function LoadCssLink(src : string | HTMLLinkElement, callback? : (HTMLLinkElement) => void) : JQuery
    {
        let link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = (<HTMLLinkElement>src).href || <string>src;

        document.getElementsByTagName('head')[0].appendChild(link);

        //This is the TRICK!!!!
        let img = document.createElement('img');
        img.onerror = function()
        {
            if (__.IsFunction(callback))
                callback(link);
        };
        img.src = link.href;

        return $(link);
    }


    /**
     * Binds a callback to be called when the CSS of the specified LINK tag(s) has been loaded
     * WARNING: linkTags MUST be already inserted in page!
     *
     * @method BindOnLoadCssLink
     * @param linkTags
     * @param callback
     */
    export function BindOnLoadCssLink(linkTags : JQuery, callback : (JQuery) => void) : void
    {
        if (__.IsJquery(linkTags) && __.IsFunction(callback))
        {
            linkTags.each(function()
            {
                let $el = $(this);

                //This is the TRICK!!!!
                let img = document.createElement('img');
                img.onerror = function()
                {
                    callback($el);
                };
                img.src = $el.attr("href");
            });
        }
    }

    /**
     * Given a TEMPLATE string and a MODEL executes the binding and returns the result string
     * Actually uses Kendo Templates syntax
     *
     * @method RenderTemplate
     * @param {JQuery | string} template The template string or a TAG holding it
     * @param model The object holding the properties to be used for the binding
     * @return {string} The result of the template execution
     */
    export function RenderTemplate(template : JQuery | string, model : any) : string
    {
        let templateText = __.IsString(template) ? template : (<JQuery>template).html();
        let templateOriFun : any = window["kendo"].template;
        let t = templateOriFun(templateText);
        return t(model);
    }



    /**
     * Replace every tag content marked with "data-localize" (encode to html) or "data-localize-html" (don't encode) attributes
     * with the App.Res localized string, or with the KEY if not found.
     *
     * @param $where {JQuery} The container to limit the search for tags to be localized
     * @method ReplaceLocalizedTagsContent
     */
    export function ReplaceLocalizedTagsContent($where : JQuery | string) : void
    {
        $(<string>$where).find("[data-localize]").each(function()
        {
            let $tag = $(this);

            let key = $tag.attr("data-localize");
            if (__.IsEmptyString(key))         //no key provided -> print empty string
                $tag.text("");
            else if (__.IsString(App.Res[key]))    //key present in App.Res -> print its localized value (also empty strings)
                $tag.text(App.Res[key]);
            else                                    //key NOT present in App.Res -> print the key
                $tag.text("[[" + key + "]]");
        });

        $(<string>$where).find("[data-localize-html]").each(function()
        {
            let $tag = $(this);

            let key = $tag.attr("data-localize-html");
            if (__.IsEmptyString(key))         //no key provided -> print empty string
                $tag.html("");
            else if (__.IsString(App.Res[key]))    //key present in App.Res -> print its localized value (also empty strings)
                $tag.html(App.Res[key]);
            else                                    //key NOT present in App.Res -> print the key
                $tag.text("[[" + key + "]]");
        });
    }

    /**
     * Notifies the user with a non modal alert message
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @param asError {boolean} If TRUE renders as a RED message
     * @method Notify
     */

    export function Notify(message : string | { MessageText : string, PropertyName : string },
                           subMessage? : string,
                           asError : boolean = false) : void
    {
        if (__.IsEmptyString(message) || window["GLB_UnloadingPage"] === true)
            return; //Probably User aborted this connection changing the page... don't show the error!

        let messageText = "";
        if (__.IsString(message))
            messageText = /*'<div class="MessageText">' + */ <string>message /*+ '</div>'*/;
        else if (typeof message === "object")
        {
            messageText = message.MessageText;

            if (message.PropertyName !== "")
            {
                let $valMsg = $("#" + message.PropertyName + "_validationMessage");
                if ($valMsg.length > 0)
                    $valMsg.text(messageText).show();
            }
            if (__.IsEmptyString(messageText) && __.IsNotEmptyString(message.PropertyName))
                messageText = App.Res.AnErrorForProperty + " '" + message.PropertyName + "' ";
        }
        if (__.IsNotEmptyString(subMessage))
            messageText += '<div class="SubMessage">' + subMessage + '</div>';

        if (messageText === "")
            return;
        /*
         if (asError)
         window["NotificateErrors"](messageText);
         else
         window["NotificateInfo"](messageText);

         $.smallBox({
         title : "",
         content : messageText,
         color : asError ? "#FF7777" : "#A6B6FF",
         timeout : 5000,
         icon : "fa fa-exclamation-circle swing animated"
         });

        $.notify(
            {
                title   : messageText,
                message : "",
                icon    : "k-icon k-i-note"
            },
            {
                type : asError ? 'danger' : 'success'
            });
        */
        (<JQueryStaticExtended>$).notify(messageText, asError ? 'error' : 'success');
    }


    /**
     *  Wraps Notify() with SUCCESS type
     *
     * @param message  {string | {MessageText : string, PropertyName : string}} ads
     * @param subMessage {string}
     * @method NotificateInfo
     */
    export function NotificateInfo(message : string | { MessageText : string, PropertyName : string },
                                   subMessage? : string) : void
    {
        Notify(message, subMessage, false);
    }


    /**
     *  Wraps Notify() with ERROR type
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @method NotificateErrors
     */
    export function NotificateErrors(message : string | { MessageText : string, PropertyName : string },
                                     subMessage? : string) : void
    {
        Notify(message, subMessage, true);
    }


    export function CloseAllNotifications() : void
    {
        $("#divSmallBoxes *").unbind();
        $("#divSmallBoxes").empty();
    }


    // /**
    //  *    Listen for error thrown by javascript
    //  *
    //  *    options.SilentMode    : if TRUE user won't be NOTIFIED (with IronLibsUi.NotificateErrors), if FALSE he will
    //  *    options.ShowFullErrorToUser : if FALSE then App.Res.JsErrorOccurred is shown. Default to __.DebugMode()
    //  *    options.SuppressError : if TRUE the error won't appear in browser console
    //  *    options.InformServer  : if TRUE a call to server is performed to log the error
    //  *
    //  * @method StartToListenForErrors
    //  */
    // export function StartToListenForErrors(options : {
    //     SilentMode? : boolean,
    //     ShowFullErrorToUser? : boolean;
    //     SuppressError? : boolean,
    //     InformServer? : boolean,
    //     FilterSilent? : (msg : string, href : string) => boolean,
    //     FilterSuppressError? : (msg : string, href : string) => boolean,
    // }) : void
    // {
    //     //MERGE OPTIONS
    //     let settings = $.extend({
    //         "SilentMode"          : false,
    //         "ShowFullErrorToUser" : __.IsDebugMode(),
    //         "SuppressError"       : false,
    //         "InformServer"        : true,
    //         "FilterSilent"        : function(args)
    //         {
    //             return settings.SilentMode;
    //         },
    //         "FilterSuppressError" : function(args)
    //         {
    //             return settings.SuppressError;
    //         }
    //     }, options || {});
    //
    //
    //     //BIND THE EVENT
    //     window.onerror = <any>function(msg : string,
    //                                    href,
    //                                    lineRow,
    //                                    lineCol,
    //                                    exception /*present in CHROME*/)
    //     {
    //         //EXCLUSIONS
    //         try
    //         {
    //             if (msg.toUpperCase().indexOf("ATTEMPT TO RUN COMPILE-AND-GO SCRIPT ON A CLEARED SCOPE") >= 0 ||
    //                 msg.toUpperCase().indexOf("NS_ERROR_NOT_AVAILABLE") >= 0)
    //                 return false;
    //         }
    //         catch (e)
    //         {/*no throw...otherwise loop! */
    //         }
    //
    //
    //         //ADD OTHER INFOS to be logged
    //         let trace : any = null;
    //         if ($.browser.msie)
    //         {
    //             try
    //             {
    //                 trace = printStackTrace({});
    //             }
    //             catch (e)
    //             {
    //             }
    //         }
    //
    //         let silent = false;
    //         try
    //         {
    //             silent = settings.FilterSilent(arguments);
    //             if (trace !== null && typeof trace === "object" && trace.length > 0)
    //             {
    //                 msg += "<br>Stack trace found: <br><br> - " + trace.join('<br> - ');
    //             }
    //             msg += "<br><br>Silent mode (kept hidden to user): " + silent;
    //             msg += "<br>Current URL: " + location.href;
    //
    //             if (__.IsNotNull(exception) && __.IsString(exception.stack))
    //                 msg += "<br>Stack trace found: <br><br> - " +
    //                     exception.stack
    //                     .replace(/ at /gi, "<br> - at ") //this if for Chrome
    //                     .replace(/@http/gi, "<br> - @http"); //this is for FireFox
    //
    //             //INFORM THE SERVER
    //             if (settings.InformServer)
    //             {
    //                 let errDescription = {
    //                     "Msg"            : msg,
    //                     "CurrentUrl"     : location.href,
    //                     "PositionInCode" : lineRow + ":" + lineCol
    //                 };
    //                 __.Net.Ajax({
    //                     url   : "/Home/LogJsError/",
    //                     data  : JSON.stringify(errDescription),
    //                     error : $.noop
    //                 });
    //             }
    //         }
    //         catch (e)
    //         {/*no throw...otherwise loop! */
    //         }
    //
    //
    //         //NOTIFICATE THE USER
    //         if (!silent)
    //         {
    //             if (settings.ShowFullErrorToUser)
    //                 NotificateErrors(App.Res.Error, msg);
    //             else
    //                 NotificateErrors(App.Res.Error);
    //         }
    //
    //         //suppress the error in console?
    //         return settings.FilterSuppressError(arguments);
    //     };
    // }


    /**
     * When user tries to change page (hitting BACK button or a link or closing the browser)
     * it's possible to PROMPT him with a message (message is NOT displayed in Firefox ver >= 4)
     *
     * @method PreventWindowExitWithMessage
     * @param msg the textual message to show NATIVELY to user
     * @param {function} [boolFunction] If passed a function to control the prevention:
     *        if returns FALSE user can exit normally, otherwise he will be prompted!
     */
    export function PreventWindowExitWithMessage(msg : string, boolFunction? : () => boolean) : void
    {
        window.onbeforeunload = function(e)
        {
            if (__.IsFunction(boolFunction) && boolFunction() == false)
                return;

            e = e || window.event;
            // For IE and Firefox ver. < 4
            if (e)
            {
                e.returnValue = msg;
            }

            // For Safari
            return msg;
        };
    }


    /**
     * Permits to exit the window without any PROMPT.
     * (Cancels registration performed with PreventWindowExitWithMessage() )
     *
     * @method AllowWindowExit
     */
    export function AllowWindowExit() : void
    {
        window.onbeforeunload = null;
    }


    /**
     * Detects the current REAL visible document size
     *
     * @return {{w : number, h : number}}
     * @method GetBrowserSize
     */
    export function GetBrowserSize() : { w : number; h : number }
    {
        if (window.innerHeight || window.innerWidth)
        {
            return {w : window.innerWidth, h : window.innerHeight};
        }
        return {
            w : document.documentElement.clientWidth,
            h : document.documentElement.clientHeight
        };
    }


    /**
     * IF What is a JQuery object returns the encoded HTML of the JQuery element itself (not only its content)
     * IF What is a string then HTML-encodes it
     *
     * @param what what to encode
     * @return {string} the encoded string
     * @method ToHtml
     */
    export function ToHtml(what : JQuery | string) : string
    {
        if (__.IsString(what))
            return $d().text(<string>what).html();
        else
            return $d().append((<JQuery>what).clone()).html();
    }



    /**
     * The default Spinner image url
     *
     * @static
     * @property {string} DefaultSpinnerImageUrl
     */
    export let DefaultSpinnerImageUrl = "/Images/spinner.gif";


    /**
     * The default Spinner SMALL image url
     *
     * @static
     * @property {string} DefaultSmallSpinnerImageUrl
     */
    export let DefaultSmallSpinnerImageUrl = "/Images/spinner-small.gif";


    /**
     * The default settings for page block effect
     *
     * @static
     * @property DefaultPageBlockingStyle
     */
    export let DefaultPageBlockingStyle = {
        message    : '<img alt="LOADING" src="' +
            DefaultSpinnerImageUrl +
            '" class="Spinner"><br><p>' +
            (__.IsNull(window["App.Res"]) ? "LOADING" : App.Res.Loading) +
            '</p>',
        css        : {border : "none", backgroundColor : "transparent", color : "#888888"},
        overlayCSS : {
            backgroundColor : '#FFF',
            opacity         : 0.5
        }
    };


    /**
     * The default settings for element block effect
     *
     * @static
     * @property DefaultElementBlockingStyle
     */
    export let DefaultElementBlockingStyle = {
        message    : '<img alt="LOADING" src="' + DefaultSmallSpinnerImageUrl + '" class="Spinner"><br>',
        css        : {border : "none", backgroundColor : "transparent", color : "#888888", width : "100%"},
        overlayCSS : {
            backgroundColor : '#FFF',
            opacity         : 0.5
        }
    };


    /**
     * Helper returning an HREF address doing nothing (useful for <a> tags)
     *
     * @method EmptyHref
     * @return {string}
     */
    export function EmptyHref() : string
    {
        return "javascript:void(0);";
    }



    /**
     * Modally LOCKS the entire page preventing any user interaction
     * @param options Eventual object to override default options
     * @method BlockPage
     */
    export function BlockPage(options? : any) : void
    {
        let opt = IronLibsUi.DefaultPageBlockingStyle;
        if (__.IsNotNullObject(options))
            opt = __.MergeObj(opt, options);
        (<JQueryStaticExtended>$).blockUI(opt);
    }



    /**
     * Unlocks the page (cancels the previous BlockPage() call)
     *
     * @method UnblockPage
     */
    export function UnblockPage() : void
    {
        (<JQueryStaticExtended>$).unblockUI();
    }


    /**
     * The currently locked (with BlockElement()) elements
     *
     * @static
     * @property BlockedElements
     */
    export let BlockedElements : HTMLElement[] = [];



    /**
     * Modally LOCKS the passed elements preventing any user interaction
     *
     * @param $els {JQuery} The elements to lock
     * @param options Eventual object to override default options
     * @method BlockElement
     */
    export function BlockElement($els : JQuery, options? : any) : void
    {
        if (!__.IsArray(IronLibsUi.BlockedElements))
            IronLibsUi.BlockedElements = [];

        let opt = IronLibsUi.DefaultElementBlockingStyle;
        if (__.IsNotNullObject(options))
            opt = __.MergeObj(opt, options);

        $els.each(function()
        {
            (<JQueryExtended>$(this)).block(opt);
            IronLibsUi.BlockedElements.push(this);
        });
    }


    /**
     * Unlocks the passed elements (cancels the previous BlockElement() call)
     *
     * @param $els {JQuery} The elements to unlock
     * @method UnblockElement
     */
    export function UnblockElement($els : JQuery) : void
    {
        $els.each(function()
        {
            (<JQueryExtended>$(this)).unblock();

            let i = $.inArray(this, IronLibsUi.BlockedElements);
            if (i > -1)
            {
                IronLibsUi.BlockedElements.splice(i, 1);
            }
        });
    }


    /**
     * Unlocks all previously locked elements (cancels the previous BlockElement() call)
     *
     * @method UnblockAllElements
     */
    export function UnblockAllElements() : void
    {
        while (IronLibsUi.BlockedElements.length > 0)
        {
            IronLibsUi.UnblockElement($(IronLibsUi.BlockedElements[0]));
        }
    }


    /**
     * Returns TRUE if the pressed key represents a NUMBER (or movement), FALSE otherwise
     *
     * @param e th Event object passed to registered callback
     * @return boolean
     * @method UserPressedANumber
     */
    export function UserPressedANumber(e : JQueryKeyEventObject) : boolean
    {
        let evt = __.IsNotNull(e) ? e : <JQueryKeyEventObject>(window.event);
        let charCode = (evt.keyCode) ? evt.keyCode : evt.which;
        return ((charCode >= 48 && charCode <= 57) ||
            (charCode >= 96 && charCode <= 105) ||
            charCode == 107 || charCode == 109 ||                                   //+ and -
            charCode == 8 || charCode == 46 ||                                      //backspace and CANC
            charCode == 37 || charCode == 38 || charCode == 39 || charCode == 40);  //ARROWS
    }


    /**
     * CommonLib library dealing with MODAL windows
     * (runs in browser, requires JQuery library)
     *
     * @class IronLibsUi.Modal
     */
    export module Modal
    {

        /**
         * The HTML template for any modal
         *
         * @static
         * @property htmlTemplate
         */
        export let htmlTemplate = `
                    <div class="modal fade">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title"></h4>
                          </div>
                          <div class="modal-body"></div>
                          <div class="modal-footer"></div>
                        </div>
                      </div>
                    </div>
                `;


        export interface IShowDialogOptions
        {
            content : string | JQuery;
            titleContent? : string | JQuery;
            showHeader? : boolean;
            showFooter? : boolean;
            footerContent? : string | JQuery;
            modalTemplate? : string;
            height? : string; //in CSS format
            width? : string; //in CSS format
            closeOnEsc? : boolean;
            closeOnOverlayClick? : boolean;
            clbkOnClose? : ($dialog : JQuery) => void;
        }


        /**
         * Opens a modal popup. Supports various parameters
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @param options {IShowDialogOptions}
         * @method ShowDialog
         */
        export function ShowDialog(options : IShowDialogOptions) : JQuery
        {
            //Merge options with DEFAULT VALUES
            let opt : IShowDialogOptions = __.MergeObj({
                showFooter          : false,
                showHeader          : true,
                closeOnEsc          : false,
                closeOnOverlayClick : false,
                modalTemplate       : Modal.htmlTemplate
            }, options);

            //create the wrapper
            let $main : JQuery = $(opt.modalTemplate).appendTo("body");

            //add the CONTENT
            if (__.IsJquery(opt.content))
            {
                $main.find(".modal-body").append(<JQuery>opt.content);
            }
            else
            {
                $main.find(".modal-body").html(__.EnsureString(<string>opt.content, "&nbsp;"));
            }

            //add HEADER and FOOTER
            if (opt.showHeader == false)
                $main.find(".modal-header").remove();
            else
            {
                if (__.IsJquery(opt.titleContent))
                    $main.find(".modal-title").append(opt.titleContent);
                else
                    $main.find(".modal-title").html(__.EnsureString(opt.titleContent, "&nbsp;"));
            }

            if (opt.showFooter == false)
                $main.find(".modal-footer").remove();
            else
            {
                if (__.IsJquery(opt.footerContent))
                    $main.find(".modal-footer").append(opt.footerContent);
                else
                    $main.find(".modal-footer").html(__.EnsureString(opt.footerContent, "&nbsp;"));
            }
            if (__.IsNotEmptyString(opt.width))
            {
                $main.find(".modal-dialog").css("width", opt.width);
            }
            if (__.IsNotEmptyString(opt.height))
            {
                $main.find(".modal-body").css("height", opt.height);
            }

            if (__.IsFunction(opt.clbkOnClose))
                $main.data("on-close", opt.clbkOnClose);

            //behaviour
            if (opt.closeOnEsc)
                $main.attr("tabindex", "-1"); //this is needed for a bootstrap modal issue on ESC press
            $main.attr("data-keyboard", opt.closeOnEsc ? "true" : "false");
            if (!opt.closeOnOverlayClick)
                $main.attr("data-backdrop", "static");


            (<JQueryExtended>$main).modal();

            return $main;
        }



        export interface IShowConfirmOptions
        {
            title? : string;
            message : string | JQuery;
            buttons : {
                text : string,
                closeOnClick : boolean,
                htmlAttributes : any,
                onClick? : (eventObject : JQuery.Event) => any;
            }[];
            otherDialogOptions? : any;
        }


        /**
         * Helper wrapping ShowDialog() showing 2 buttons
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @method ShowConfirm
         * @param msg {string | JQuery}
         * @param onYes {Function}
         * @param onNo {Function}
         * @param yesText {string}
         * @param noText {string}
         * @param titleText {string}
         * @param otherDialogOptions
         */
        export function ShowConfirm(msg : string | JQuery,
                                    onYes? : (eventObject : JQueryEventObject) => any,
                                    onNo? : (eventObject : JQueryEventObject) => any,
                                    yesText? : string,
                                    noText? : string,
                                    titleText? : string,
                                    otherDialogOptions? : any) : JQuery;
        /**
         * Helper wrapping ShowDialog() showing 2 buttons
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @method ShowConfirm
         * @param options {IShowConfirmOptions}
         */
        export function ShowConfirm(options : IShowConfirmOptions) : JQuery;
        export function ShowConfirm() : JQuery
        {
            let opt : IShowConfirmOptions;

            if (__.IsString(arguments[0]) || __.IsJquery(arguments[0]))
            {
                //FIRST function signature case
                opt = {
                    title              : __.EnsureString(arguments[5], App.Res.ConfirmationRequest),
                    message            : arguments[0],
                    buttons            : [
                        {
                            text           : __.EnsureString(arguments[4], App.Res.No),
                            closeOnClick   : true,
                            htmlAttributes : {"class" : "btn btn-w-m btn-outline btn-default"},
                            onClick        : arguments[2]
                        }, {
                            text           : __.EnsureString(arguments[3], App.Res.Yes),
                            closeOnClick   : true,
                            htmlAttributes : {"class" : "btn btn-w-m btn-primary"},
                            onClick        : arguments[1]
                        }
                    ],
                    otherDialogOptions : arguments[6]
                };
            }
            else
            {
                //SECOND function signature case
                opt = __.MergeObj({
                    title   : App.Res.ConfirmationRequest,
                    buttons : []
                }, arguments[0]);
            }

            let $buttons = $();
            opt.buttons.forEach(function(b)
            {
                let $b = $("<button>" + b.text + "</button>");
                if (__.IsNotNullObject(b.htmlAttributes))
                    $b.attr(b.htmlAttributes);

                //FIRST bind the passed function (which can ABORT event bubbling!!!)
                if (__.IsFunction(b.onClick))
                    $b.click(b.onClick);

                //The our CLOSE function
                if (b.closeOnClick)
                    $b.click(function(e)
                    {
                        HideDialog();
                    });

                $buttons = $buttons.add($b);
            });



            let dialogOpts : IShowDialogOptions = {
                content       : opt.message,
                showHeader    : true,
                showFooter    : true,
                titleContent  : opt.title,
                footerContent : $buttons,
                closeOnEsc    : true
            };

            //merge other eventual options
            if (__.IsNotNullObject(opt.otherDialogOptions))
                dialogOpts = __.MergeObj(dialogOpts, opt.otherDialogOptions);

            setTimeout(() =>
            {
                $(".modal-footer .btn").last().focus();
            }, 510);

            return ShowDialog(dialogOpts);
        }


        /**
         *
         * In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure.
         *
         * @param msg
         * @param onOk
         * @param onCancel
         * @param yesText
         * @param cancelText
         * @param titleText
         * @param showCancel
         * @param valueRequired
         * @param valueObscured
         */
        export function ShowPrompt(msg : string,
                                   onOk? : (val : string, eventObject : JQuery.Event) => any,
                                   onCancel? : (eventObject : JQuery.Event) => any,
                                   yesText? : string,
                                   cancelText? : string,
                                   titleText? : string,
                                   showCancel? : boolean,
                                   valueRequired? : boolean,
                                   valueObscured? : boolean);
        export function ShowPrompt(options : IShowConfirmOptions);
        export function ShowPrompt()
        {
            let opt : IShowConfirmOptions;
            let mainArgs = arguments;

            let obscured = !!arguments[8];

            if (__.IsString(arguments[0]))
            {
                //FIRST function signature case
                opt = {
                    title   : __.EnsureString(arguments[5], App.Res.ConfirmationRequest),
                    message : __.EnsureString(arguments[0], ""),
                    buttons : []

                };
                if (__.IsNull(arguments[6]) || arguments[6])
                {
                    opt.buttons.push({
                        text           : __.EnsureString(arguments[4], App.Res.Cancel),
                        closeOnClick   : true,
                        htmlAttributes : {"class" : "btn btn-outline pull-left"},
                        onClick        : arguments[2]
                    });
                }
                opt.buttons.push({
                    text           : __.EnsureString(arguments[3], "Ok"),
                    closeOnClick   : true,
                    htmlAttributes : {"class" : "btn btn-primary BtnOk"},
                    onClick        : function(e)
                    {
                        let $b = $(this);
                        let $m = $b.parents(".modal-content:eq(0)");
                        let $i = $m.find(".TxtPrompt");

                        //VALIDATE
                        let val = $i.val();
                        if (__.IsEmptyString(val) && __.IsNull(mainArgs[7]) || mainArgs[7])
                        {
                            $m.toggleClass("has-error", true);
                            e.stopImmediatePropagation();
                            return;
                        }

                        //pass the value to callback
                        if (__.IsFunction(mainArgs[1]))
                            mainArgs[1](val, e);
                    }
                });

            }
            else
            {
                //SECOND function signature case
                opt = __.MergeObj({
                    title   : App.Res.ConfirmationRequest,
                    buttons : []
                }, arguments[0]);
            }
            opt.message = `<h4>${opt.message}</h4><br>
                           <input type="${obscured ? "password" : "text"}" class="TxtPrompt form-control">
                           <span></span>`;

            let $buttons = $();
            opt.buttons.forEach(function(b)
            {
                let $b = $("<button><span>" + b.text + "</span></button>");
                $b.attr(b.htmlAttributes);

                //FIRST bind the passed function (which can ABORT event bubbling!!!)
                if (__.IsFunction(b.onClick))
                    $b.click(b.onClick);

                //Our CLOSE function
                if (b.closeOnClick)
                    $b.click(function(e)
                    {
                        HideDialog();
                    });

                $buttons = $buttons.add($b);
            });

            ShowDialog({
                content       : opt.message,
                showHeader    : true,
                showFooter    : true,
                titleContent  : opt.title,
                footerContent : $buttons,
                closeOnEsc    : true
            });

            setTimeout(function()
            {
                $(".modal .modal-content:eq(0) .TxtPrompt").focus().keydown(function(e)
                {
                    if (e.keyCode == 13)
                    {
                        let $btn = $(this).parents(".modal-content:eq(0)").find(".BtnOk");
                        $btn.click();
                    }
                });
            }, 600);

        }

        /**
         * Hides a modal shown with the "ShowXXX" method
         *
         * @method HideDialog
         * @return {boolean} TRUE if the modal has been FOUND and HID, FALSE otherwise
         */
        export function HideDialog() : boolean
        {
            let $dialog = $("body > .modal");
            if ($dialog.length > 0)
            {
                let plugin = $dialog.data('bs.modal');
                if (__.IsNotNull(plugin) && __.IsFunction(plugin.hide))
                {
                    //gracefully HIDE the dialog with animation
                    plugin.hide();

                    return true;
                }
            }
            return false;
        }


        /**
         * Unbinds and destroys all dialogs, also if closed (they remain in the body)
         *
         * @method DestroyAllDialogs
         */
        export function DestroyAllDialogs()
        {
            $("body > .modal").unbind();
            $("body > .modal, body > .modal-backdrop").unbind().remove();
        }

        if (__.IsFunction((<JQueryExtended>$.fn).modal))
            $(document).ready(function()
            {
                //IMPORTANT: Bootstrap modal plugins NEVER destroys its divs appended to the body.
                // To keep things clean we destroy them after the hiding transition

                __.WrapFun(function()
                {
                    let $dialog = this.$dialog.parents(".modal:eq(0)");
                    let onClose = $dialog.data('on-close');
                    if (__.IsFunction(onClose))
                        onClose($dialog);

                    //fix
                    let plugin : any = $dialog.data('bs.modal');
                    if (__.IsNull(plugin))
                        return;
                    plugin["resetAdjustments"]();
                    plugin["resetScrollbar"]();
                    $("body").removeClass("modal-open");

                    //DESTROY IT
                    let modalClass : any = (<JQueryExtended>$.fn).modal.constructor;
                    let duration = __.IsNumber(modalClass.TRANSITION_DURATION)
                        ? modalClass.TRANSITION_DURATION + 20
                        : 550;
                    setTimeout(IronLibsUi.Modal.DestroyAllDialogs, duration);
                }, "hide", (<JQueryExtended>$.fn).modal.constructor.prototype, false);
            });
    }
}

