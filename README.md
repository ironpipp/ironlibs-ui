# IronLibs

Ironpipp's useful utilities for both nodejs, browser with jQuery or Angular. 

This repo holds browser-only utilities.

## Features
- Fully typed
- by default no dependencies are needed. Are required ONLY when used
    - **block-ui** for IronLibsUi.Block... and IronLibsNet
    - **jquery-cookie** for IronLibsNet.Get/SetCookie

So install proper packages based on your necessities.

### How to build and test
     > npm run build
